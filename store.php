<?php
/*
 *Plugin Name: POS Store
 *Plugin URI :
 *Description: Woocommerce Plugin connect with POS system to enhance product sync, stores sync, show quantity from pos, order sync functionality etc..
 *Author: Asif
 *Version: 2.0
 *Author URI :	
 */

/*
POS Store is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.
*/

// PLUGIN ACTIVATION HOOK
register_activation_hook( __FILE__, 'plugin_install' );
// PLUGIN INSTALL FUNCTION
function plugin_install() {
    /**
    * Check if WooCommerce are active
    **/
    if ( !in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

        // Deactivate the plugin
        deactivate_plugins( __FILE__ );

        // Throw an error in the wordpress admin console
        $error_message = __('POS Store plugin requires WooCommerce to be installed and active. You can download <a href="https://woocommerce.com/" target="_blank">WooCommerce here</a>.', 'woocommerce');
        die($error_message);
    }else{
        register_tables();
        wp_schedule_event( time(), 'every_sixty_minutes', 'pos_store_order_sync' );
        if (! wp_next_scheduled ( 'pos_store_order_sync' )) {
            wp_schedule_event( time(), 'every_five_minutes', 'pos_store_order_sync');
        }
        if (!file_exists(plugin_dir_path(__FILE__).'logs')) {
            mkdir(plugin_dir_path(__FILE__).'logs', 0777, true);
        }
    }
}

if (!defined('ABSPATH')) { exit; }

if (!defined('POS_PLUGIN_VERSION')) {
    define('POS_PLUGIN_VERSION', '2.0');
}
set_time_limit(0);
error_reporting(1);
/**
** Check if WooCommerce is active
**/
if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    add_action('admin_notices', 'wps_store_inactive_notice');
    return;
}

function wps_store_inactive_notice() {
    if ( current_user_can( 'activate_plugins' ) ) :
        if ( !class_exists( 'WooCommerce' ) ) :
            ?>
            <div id="message" class="error">
                <p>
                    <?php
                    printf(
                        __('%s requires %sWooCommerce%s to be active.', 'pos-pickup-store'),
                        '<strong>POS Store</strong>',
                        '<a href="http://wordpress.org/plugins/woocommerce/" target="_blank" >',
                        '</a>'
                    );
                    ?>
                </p>
            </div>      
            <?php
        endif;
    endif;
}

// Set Woocommerce TimeZone
$timezone = get_option('timezone_string');
date_default_timezone_set($timezone);

// Add Settings action links
function pos_store_links($links) {
    $id = "pos_store";
    $plugin_links = array(
        '<a href="' . admin_url('admin.php?page=wc-settings&tab=shipping&section=' . $id) . '">' . __('Shipping Settings') . '</a>',
        '<a href="' . admin_url('admin.php?page=sync_settings&tab=1').'">' . __('API Settings') . '</a>',
    );
    // Merge our new link with the default ones
    return array_merge($plugin_links, $links);    
}
add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'pos_store_links');

function store_error_log($type,$msg){
    $pluginlog = plugin_dir_path(__FILE__).'logs/debug.log';
    $logDate = date('d-M-Y h:i:s A (e)');
    $message = "[ $logDate ] : ".ucwords($type." : ".strtolower($msg)).PHP_EOL;
    error_log($message, 3, $pluginlog);
}

// CREATEING PLUGIN TABLE'S IN DB
function register_tables(){
    global $wpdb;
    $charset_collate = $wpdb->get_charset_collate();
    $table_name = $wpdb->prefix . 'POS_Stores';
    $table_save_data = $wpdb->prefix . 'POS_Stores_sync_setting';

    $sql = "CREATE TABLE $table_name (
    id int NOT NULL AUTO_INCREMENT,
    pos_id int,
    location_id varchar(255),
    name varchar(255),
    country varchar(255),
    state varchar(255),
    landmark varchar(255),
    city varchar(255),
    zip_code int,
    mobile varchar(50),
    alternate_number varchar(50),
    email varchar(255),
    file varchar(255),
    time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UNIQUE KEY id (id)
    ) $charset_collate;";

    $save_sql = "CREATE TABLE $table_save_data (
    id int NOT NULL AUTO_INCREMENT,
    url varchar(255),
    key1 varchar(255),
    key2 varchar(255),
    dfstore varchar(255),
    time datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    UNIQUE KEY id (id)
    ) $charset_collate;";

    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $wpdb->query($sql);
    }

    if($wpdb->get_var("SHOW TABLES LIKE '$table_save_data'") != $table_save_data) {
        $wpdb->query($save_sql);
    }
}
// CREATING MAIN MENUS
function store_options_page() {
    add_menu_page(
        'Store',
        'Store',
        'manage_options',
        'store',
        'store_options_page_html',
        plugin_dir_url(__FILE__) . 'assets/images/store_icon 20x20.png',
        20
    );

    add_submenu_page(
        'store',
        'Sync',
        'Sync',
        'manage_options',
        'sync_settings',
        'sync_settings'
    );

    add_submenu_page(
        'store',
        'Logs',
        'Logs',
        'manage_options',
        'store_log',
        'store_log'
    );
}
// ADD MENU HOOK
add_action( 'admin_menu', 'store_options_page' );
// MENU STORE SHOW FUNCTION
function store_options_page_html(){
    include_once 'includes/show-stores.php';
}
// MENU SHOW SETTING'S, CRON, MANUAL SYNC
function sync_settings(){
    include_once 'includes/sync-settings.php';
}
function store_log(){
    include_once 'includes/show-log.php';
}
//STORE CUSTOM FUNCTIONS
include_once 'includes/store-custom-functions.php';
// REGISTER CRON INTERVALS
function pos_store_add_cron_intervals($schedules){
  // add weekly and monthly intervals
    $schedules['weekly'] = array(
        'interval' => 604800,
        'display' => __('Once Weekly')
    );

    $schedules['monthly'] = array(
        'interval' => 2635200,
        'display' => __('Once a month')
    );
    $schedules['every_five_minutes'] = array(
        'interval'  => 300,
        'display'   => __( 'Every 5 Minutes', 'textdomain' )
    ); 
    return $schedules;
}
add_filter( 'cron_schedules', 'pos_store_add_cron_intervals');

// CRON REGISTER
add_action('pos_store_daily','sync_store_data');
add_action('pos_store_twicedaily_1','sync_store_data');
add_action('pos_store_twicedaily_2','sync_store_data');
add_action('pos_store_weekly','sync_store_data');
add_action('pos_store_monthly','sync_store_data');
// ORDER SYNC CRON
add_action('pos_store_order_sync','store_order_send_to_pos_cron');

function store_order_send_to_pos_cron(){
    store_error_log("message","Order sync via cron job start");
    $url = plugin_dir_url( __FILE__ ).'includes/order-sync/order-sync-cron.php';
    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => -1,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => array(),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if($err){
        store_error_log("error","cURL Error #:" . $err);
    }
}
// CRON HIT FUNCTION
function sync_store_data(){
    store_error_log('message',"cron sync start at ".date("d-m-Y H:i:s A"));
    $url = plugin_dir_url( __FILE__ ).'includes/sync-function.php?type=sync-stores&sync=cron';
    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => -1,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => array(),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    if($err){
        store_error_log("error","cURL Error #:" . $err);
    }

    $url = plugin_dir_url( __FILE__ ).'includes/sync-function.php?type=sync-stores-data&sync=cron';
    // error_log("Accessing file at url ".$url);

    $curl = curl_init();
    curl_setopt_array($curl, array(
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => -1,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => array(),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);
    if($err){
        store_error_log("error","cURL Error #:" . $err);
    }
    store_error_log("message","cron sync sucessfully end at ".date("d-m-Y H:i:s A"));
}

//UNINSTALL PLUGIN
register_uninstall_hook(__FILE__, 'store_plugin_uninstall');
function store_plugin_uninstall() {
    global $wpdb;
    $table_stores = $wpdb->prefix . 'POS_Stores';
    $table_setting = $wpdb->prefix . 'POS_Stores_sync_setting';

    $remove_Stores = "drop table if exists $table_stores";
    $remove_Setting = "drop table if exists $table_setting";

    $wpdb->query($remove_Stores);
    $wpdb->query($remove_Setting);
}