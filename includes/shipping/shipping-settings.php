<?php

function pos_store_shipping_method( $methods ) {
    $methods['pos_store'] = 'POS_STORE';

    return $methods;
}
add_filter('woocommerce_shipping_methods', 'pos_store_shipping_method');

/**
** Declare Shipping Method
**/
function pos_store_shipping_method_init() {
    class POS_STORE extends WC_Shipping_Method {
        /**
         * Constructor for your shipping class
         *
         * @access public
         * @return void
         */
        public function __construct() {
            $this->id = 'pos_store';
            $this->method_title = __('POS Pickup Store');
            $this->method_description = __('Lets users to choose a pos store to pick up their products');

            $this->init();
            // $this->includes();
        }

        // public function includes() {}

        /**
         * Init your settings
         *
         * @access public
         * @return void
         */
        function init() {
            // Load the settings API
            $this->init_form_fields(); // This is part of the settings API. Override the method to add your own settings
            $this->init_settings(); // This is part of the settings API. Loads settings you previously init.

            // Turn these settings into variables we can use
            foreach ( $this->settings as $setting_key => $value ) {
                $this->$setting_key = $value;
            }

            // Save settings in admin if you have any defined
            add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
            add_filter('woocommerce_get_order_item_totals', array($this, 'wc_reordering_order_item_totals'), 10, 3);
        }

        public function init_form_fields() {
            $this->form_fields = array(
                'enabled' => array(
                    'title' => __( 'Enable/Disable', 'woocommerce' ),
                    'type' => 'checkbox',
                    'label' => __( 'Enable', 'woocommerce' ),
                    'default'  => 'yes',
                    'description' => __( 'Enable/Disable shipping method' ),
                    'desc_tip'    => true
                ),
                'enable_store_select' => array(
                    'title' => __( 'Enable stores in checkout' ),
                    'type' => 'checkbox',
                    'label' => __( 'Enable', 'woocommerce' ),
                    'default'  => 'yes',
                    'description' => __( 'Shows select field to pick a store in checkout' ),
                    'desc_tip'    => true
                ),
                'title' => array(
                    'title' => __( 'Shipping Method Title' ),
                    'type' => 'text',
                    'description' => __( 'Label that appears in checkout options' ),
                    'default' => __( 'Collect From Store' ),
                    'desc_tip'    => true
                ),
                'checkout_notification' => array(
                    'title' => __( 'Checkout notification' ),
                    'type' => 'textarea',
                    'description' => __( 'Message that appears next to shipping options in Checkout page' ),
                    'default' => __( '' ),
                    'desc_tip'    => true
                ),
                'plugin_version' => array(
                    'type' => 'plugin_version',
                ),
            );
        }

        public function is_available( $package ) {
            $is_available = ($this->enabled == 'yes') ? true : false;

            return apply_filters( 'woocommerce_shipping_' . $this->id . '_is_available', $is_available, $package, $this );
        }

        /**
         * calculate_shipping function.
         *
         * @access public
         * @param mixed $package
         * @return void
         */
        public function calculate_shipping( $package = array() ) {
            $rate = array(
                'id' => $this->id,
                'label' => $this->title,
                'cost' => (!empty($this->costs) && $this->costs_per_store != 'yes') ? apply_filters('pos_shipping_costs', $this->costs) : 0,
                'package' => $package,
                'calc_tax' => 'per_order' // 'per_item'
            );

            // Register the rate
            $this->add_rate( $rate );
        }

        public function generate_store_default_html() {
            ob_start();
            ?>
            <tr valign="top">
                <th scope="row" class="titledesc"><?php _e('Default store'); ?>:</th>
                <td class="forminp">
                    <p><?php
                        echo sprintf(__('Find this option in <a href="%s" target="_blank">the Customizer</a>'), admin_url('/customize.php?autofocus[section]=pos_store_customize_section'));
                    ?></p>
                </td>
            </tr>
            <?php
            return ob_get_clean();
        }

        public function generate_plugin_version_html() {
            ob_start();
            ?>
            <tr valign="top">
                <td colspan="2" align="right">
                    <p><em><?php echo sprintf(__('Version %s', $this->id), POS_PLUGIN_VERSION); ?></em></p>
                </td>
            </tr>
            <?php
            return ob_get_clean();
        }

        public function wc_reordering_order_item_totals($total_rows, $order, $tax_display) {
            /* Update 1.5.9 */
            $order_id = $order->get_id();
            $store = get_post_meta($order_id, '_shipping_pickup_pos_stores', true);
            $formatted_title = (!empty($this->costs) && $this->costs_per_store != 'yes') ? $this->title . ': ' . wc_price($this->costs) : $this->title;

            if($order->has_shipping_method($this->id) && !empty($store)) {
                foreach ($total_rows as $key => $row) {
                    $new_rows[$key] = $row;
                    if($key == 'shipping') {
                        $new_rows['shipping']['value'] = $formatted_title;
                        $new_rows[$this->id] = array(
                            'label' => __('Pickup Store:') . $this->checkout_notification,
                            'value' => $store
                        );
                    }
                }
                $total_rows = $new_rows;
            }

            return $total_rows;
        }
    }
    new POS_STORE();
}
add_action('init', 'pos_store_shipping_method_init');

/**
** Returns the main instance for POS_STORE class
**/
function pos_store() {
    return new POS_STORE();
}

function pos_store_get_store_admin(){
    global $wpdb;
    $table_name = $wpdb->prefix . 'POS_Stores';
    $results = $wpdb->get_results("SELECT * FROM $table_name");
    foreach ($results as $key) {
        $options[$key->pos_id] = __( $key->name );
    }
    return $options;
}

/**
** No stores message
**/
function pos_no_stores_availables_message() {
    return __('There are not available stores. Please choose another shipping method.');
}

/**
** Get chosen shipping method
**/
function pos_get_chosen_shipping_method() {
    $chosen_methods = WC()->session->get('chosen_shipping_methods');

    return $chosen_methods[0];
}

/**
** Store table row layout
**/
function pos_store_row_layout() { 
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    $chosen_shipping = pos_get_chosen_shipping_method();
    $no_stores_message = pos_no_stores_availables_message();
    $checkout_notification = POS_STORE()->checkout_notification;
    $store_default = apply_filters('pos_first_store', get_user_meta($user_id, '_shipping_pickup_pos_stores', true));
    if ($chosen_shipping === POS_STORE()->id) :
        $store_default = !empty($post_data['shipping_pickup_stores']) ? $post_data['shipping_pickup_stores'] : $store_default;
        $ship_to_store = !empty($post_data['shipping_by_store']) ? $post_data['shipping_by_store'] : '';
        $total_costs = 0;
        ?>
    
        <tr class="shipping-pickup-store">
            <?php if(count(pos_store_get_store_admin()) > 0) :
                if(POS_STORE()->enable_store_select == 'yes' || is_cart()) : ?>
                    <th><strong><?php echo __('Select Store') ?></strong></th>
                    <td>
                        <select id="shipping-pickup-store-select" class="<?= (POS_STORE()->costs_per_store == 'yes') ? 'pos-costs-per-store' : 'pos-no-costs' ?>" name="shipping_pickup_stores" data-store="<?= $store_default ?>">
                            <!-- <?php //if(empty($store_default)) : ?> -->
                                <option value=""><?= __('Select a store') ?></option>
                            <!-- <?php //endif; ?> -->
                            <?php
                            $cost = "";
                            foreach (pos_store_get_store_admin(true) as $post_id => $store) {

                                // $store_shipping_cost = POS_STORE()->costs;
                                // $cost = (!empty($store_shipping_cost) && POS_STORE()->costs_per_store == 'yes') ? $store_shipping_cost : 0;
                                // $formatted_title = ($cost > 0) ? $store . ': ' . wc_price($cost) : $store;
                                // $ship_to_store = ($store_default == $store) ? $cost : $ship_to_store;
                                ?>
                                <option data-cost="<?= $cost ?>" value="<?= $store; ?>" <?php selected($store, $store_default); ?>><?= $store; ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        <input type="hidden" id="store_shipping_cost" name="shipping_by_store" value="<?= $ship_to_store ?>">
                        <?php if($ship_to_store > 0 && !isset($post_data['shipping_pickup_stores'])) : ?>
                            <script type="text/javascript">
                                jQuery('body').trigger('update_checkout');
                            </script>
                        <?php endif; ?>
                    </td>
                <?php elseif(!empty($store_default) && in_array($store_default, pos_store_get_store_admin())) : ?>
                    <th><strong><?php echo __('Store') ?></strong></th>
                    <td>
                        <strong><?= $store_default ?></strong>
                        <input type="hidden" name="shipping_pickup_stores" value="<?= $store_default; ?>">
                    </td>
                <?php else :  ?>
                    <td colspan="2">
                        <span class="no-store-default"><?= pos_no_stores_availables_message(); ?></span>
                    </td>
                <?php endif;
            else : ?>
                <td colspan="2">
                    <span class="no-store-available"><?= pos_no_stores_availables_message(); ?></span>
                </td>
            <?php endif; ?>
        </tr>

        <?php if(!empty($checkout_notification)) : ?>
            <tr class="shipping-pickup-store">
                <td colspan="2">
                    <span class="store-message"><?= sanitize_textarea_field($checkout_notification) ?></span>
                </td>
            </tr>
        <?php
        endif;
    endif;
}
add_action('woocommerce_after_shipping_calculator', 'pos_store_row_layout');
add_action('woocommerce_review_order_after_shipping', 'pos_store_row_layout');

/**
** Save the custom field.
**/
function pos_store_save_order_meta($order_id) {
    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;

    $store = isset( $_POST['shipping_pickup_stores'] ) ? $_POST['shipping_pickup_stores'] : '';

    if ( $store ) {
        update_post_meta($order_id, '_shipping_pickup_pos_stores', $store);
        update_user_meta($user_id, '_shipping_pickup_pos_stores', $store);
    }
}
add_action('woocommerce_checkout_update_order_meta', 'pos_store_save_order_meta');

/**
** Check store field before allow checkout to proceed.
**/
function pos_store_validate_checkout($data) {
    if (isset($_POST['shipping_pickup_stores']) && empty($_POST['shipping_pickup_stores'])) {
        wc_add_notice(__('You must either choose a store or use other shipping method'), 'error');
    }

    if (in_array('pos_store', $data['shipping_method']) && count(pos_store_get_store_admin()) == 0) {
        wc_add_notice(pos_no_stores_availables_message(), 'error');
    }
}
add_action('woocommerce_after_checkout_validation', 'pos_store_validate_checkout', 10, 2);

/**
** Remove cart shipping label
**/
function pos_shipping_method_label( $label, $method ) {
    if($method->method_id == 'pos_store' && ((int)$method->get_cost()) == 0) {
        $label = $method->get_label();
    }

    return $label;
};
add_filter('woocommerce_cart_shipping_method_full_label', 'pos_shipping_method_label', 10, 2);

/**
** Add selected store to billing details, admin page
**/
function pos_show_store_in_admin($order) {
    $order_id = $order->get_id();
    $store = (!empty(get_post_meta($order_id, '_shipping_pickup_pos_stores', true))) ? get_post_meta($order_id, '_shipping_pickup_pos_stores', true) : '';
    
    if(!empty($store)) :
        ?>
        <p>
            <strong class="title"><?php echo __('Pickup Store') . ':' ?></strong>
            <span class="data"><?= $store ?></span>
        </p>
        <?php
    endif;
}
add_action('woocommerce_admin_order_data_after_billing_address', 'pos_show_store_in_admin');

// CUSTOM SCRIPT TO REMEMBER THE STORE ON CHECKOUT
add_action('wp_footer','custom_jquery_add_to_cart_script');
function custom_jquery_add_to_cart_script(){
    if (is_cart()): // Only for archives pages
        ?>
            <script type="text/javascript">
                // Ready state
                (function($){ 
                    $('#shipping-pickup-store-select').change(function(){
                        data = this.value;
                        console.log(data);
                        $.ajax({
                            type: 'POST',
                            url: "<?php echo plugin_dir_url( __FILE__ ).'ajax-save-meta.php'; ?>",
                            data: {data : data},
                            dataType: "text",
                            success: function(resultData) { 
                                //alert("Save Complete") 
                            }
                        });
                    });

                })(jQuery); // "jQuery" Working with WP (added the $ alias as argument)
            </script>
        <?php
    endif;
}

add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) {
    if ( ! $order_id ) {
        return;
    }

    $pickStore = get_post_meta($order_id, '_shipping_pickup_pos_stores', true);
    if (!empty($pickStore)) {
        add_action('wp_footer','custom_css_shipping');
        function custom_css_shipping(){
            ?>
            <style type="text/css">
                .woocommerce-customer-details{
                    display: none !important;
                }
            </style>
            <?php
        }
    }
}
