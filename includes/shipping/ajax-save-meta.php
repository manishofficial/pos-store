<?php
	$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
	require_once $root.'/wp-load.php' ;

    $current_user = wp_get_current_user();
    $user_id = $current_user->ID;
    $store = isset( $_POST['data'] ) ? $_POST['data'] : '';
    if ( $store ) {
        update_user_meta($user_id, '_shipping_pickup_pos_stores', $store);
    }
?>