<?php
	$columns = array(
		'posid' => 'POS Id',
		'name' => 'Name',
		'mobile' => 'Mobile',		
		'alternate_number' => 'Alternate Number',
		'email' => 'Email',
		'state' => 'State',
		'city' => 'City',
		'country' => 'Country',
		// 'action' => 'Action'
	);
	register_column_headers('pro-list-site', $columns);	
?>
<div class="wrap">  
	<?php 	echo "<h2>" . __( 'List All Stores' ) . "</h2>"; ?>
	<?php echo woo_api_notice(); ?>
	<table class="widefat page fixed" cellspacing="0" width="75%">
   		<thead>
     		<tr> <?php print_column_headers('pro-list-site'); ?> </tr>
   		</thead>
	   	<br/>
	  	<tfoot>
	   		<tr> <?php print_column_headers('pro-list-site', false); ?> </tr>
	   	</tfoot>
	   	<tbody>
   			<?php
				global $wpdb;
				$table_name = $wpdb->prefix . 'POS_Stores';
				$results = $wpdb->get_results("SELECT * FROM $table_name");
				if(count($results) > 0)
				{
					foreach ($results as $store) {
			  			$tableRow = array_keys((array)$store);
			  			unset($tableRow[0],$tableRow[6],$tableRow[8],$tableRow[12],$tableRow[13]);
					}

					foreach($results as $store){ ?>
						<tr>
							<td><?php echo $store->pos_id; ?></td>
							<td><?php echo $store->name; ?></td>
							<td><?php echo $store->mobile; ?></td>
							<td><?php echo $store->alternate_number; ?></td>
							<td><?php echo $store->email; ?></td>
							<td><?php echo $store->state; ?></td>
							<td><?php echo $store->city; ?></td>
							<td><?php echo $store->country; ?></td>
							<!-- <td><a href="<?php //echo plugin_dir_url(__FILE__); ?>delete-store.php?storeid=<?php //echo $store->pos_id; ?>">Delete</a></td> -->
						</tr>
			<?php	}			
				}else{
					echo "<tr>";
						echo "<td colspan='7'>";
							echo "Please <a href='".admin_url()."/admin.php?page=sync_settings'>click here</a> to sync stores";
						echo "</td>";
					echo "</tr>";
				}
			?>
   </tbody>
 </table>
</div>

<p style="text-align: right; padding-right: 30px;"><em><?php echo sprintf(__('Version %s', 'pos_store'), POS_PLUGIN_VERSION); ?></em></p>
