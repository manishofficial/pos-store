<?php
  global $wpdb;
  $table_name = $wpdb->prefix . 'POS_Stores';
  $gif = dirname(plugin_dir_url( __FILE__ )).'/assets/images/';
  $table_save_data = $wpdb->prefix . 'POS_Stores_sync_setting';
  $check_data_exists = $wpdb->get_results("SELECT * FROM $table_save_data");
  $getall_store = $wpdb->get_results("SELECT * FROM $table_name");

  if (!empty($check_data_exists)) {
    $url = $check_data_exists[0]->url;
    $key1 = $check_data_exists[0]->key1;
    $key2 = $check_data_exists[0]->key2;
    $dfstore = $check_data_exists[0]->dfstore;
  }else{
    $url = '';
    $key1 = '';
    $key2 = '';
    $dfstore = '';
  }
?>

<!-- TAB NAVBAR POS STORE -->
<h2 class="nav-tab-wrapper">
  <button id="1" class="pos-tab nav-tab nav-tab-active">API Settings</button>
  <button id="2" class="pos-tab nav-tab">Setup Cron</button>
  <button id="3" class="pos-tab nav-tab">Manual Sync</button>
  <!-- <button id="4" class="pos-tab nav-tab">Select Store</button> -->
</h2>
<?php echo woo_api_notice(); ?>
<!-- SETTING TAB -->
<div id="setting">
  <h1>API Settings</h1>
  <div id="myAlert"></div>
  <div class="">
    <form class="was-validated" method="post">
      <table class="api-setting-table">
          <tr>
            <td>
              <label for="url">Url : </label>
            </td>
            <td>
              <input type="text" class="input" id="url" placeholder="Enter pos site url" name="url" value="<?php echo $url; ?>" required>
            </td>
          </tr>
          <tr>
            <td>
              <label for="key1" >Consumer key : </label>
            </td>
            <td>
              <input type="text" class="input" id="key1" placeholder="Enter key1" name="key1" value="<?php echo $key1; ?>" required>
            </td>
          </tr>
          <tr>
            <td>
              <label for="key2">Consumer secret : </label>
            </td>
            <td>
              <input type="text" class="input" id="key2" placeholder="Enter key2" name="key2" value="<?php echo $key2; ?>" required>
            </td>
          </tr>
      </table>
      <button type="submit" class="button-primary" id="save">Save</button>
    </form>
  </div>
</div>

<!-- CRON TAB -->
<div id="cron" style="display: none;">
  <?php include_once 'cron/cron-form.php'; ?>
</div>

<!-- MANUAL SYNC TAB -->
<div id="sync" style="display: none;">
  <div>
    <h1>Manual Sync</h1>
  </div>
  <div>
    <button id="store" class="button-primary">Sync Stores</button>
    <button id="products" class="button-primary">Sync Store Product Data</button>
  </div>
  <div style="display: none;" id="gif">
    <p>Please do not refresh this window or click the back button on your browser.</p>
    <img src="<?php echo $gif.'loadingdata.gif'; ?>" width="10%">
  </div>
  <div id="sync-msg">
  </div>
</div>
<p style="text-align: right; padding-right: 30px;"><em><?php echo sprintf(__('Version %s', 'pos_store'), POS_PLUGIN_VERSION); ?></em></p>
<style type="text/css">
  .nav-tab:focus{
    outline: none;
  }
  .input {
    width: 100%;
    padding: 6px 12px;
  }
  .api-setting-table{
    width: 40%;
    font-size: 14px;
    padding-bottom: 1%;
  }
  .default-table{
    width: 40%;
    font-size: 14px;
    padding-bottom: 1%;
  }
</style>
<script type="text/javascript">
  jQuery('#sync').hide();
  jQuery('#cron').hide();
  jQuery('#selectstore').hide();
  jQuery('#setting').show();
  let searchParams = new URLSearchParams(window.location.search)
  if (searchParams.has('tab')) {
    let param = searchParams.get('tab');
    jQuery(".pos-tab").removeClass("nav-tab-active");
    jQuery('#'+param).addClass("nav-tab-active");
    changeTab(param);
  }else{
    updateURL(1);
  }
  jQuery('.nav-tab').click(function(){
    var tab = jQuery(this).attr('id');
    updateURL(tab);
    jQuery(".pos-tab").removeClass("nav-tab-active");
    jQuery(this).addClass("nav-tab-active");
    changeTab(tab);
  });
  //TAB CHANGE OR UPDATE URL
  function changeTab(tab){
    if (tab == 1) {
      jQuery('#sync').hide();
      jQuery('#cron').hide();
      jQuery('#selectstore').hide();
      jQuery('#setting').show();
    }
    if (tab == 2) {
      jQuery('#sync').hide();
      jQuery('#setting').hide();
      jQuery('#selectstore').hide();
      jQuery('#cron').show();
    }
    if (tab == 3) {
      jQuery('#setting').hide();
      jQuery('#cron').hide();
      jQuery('#selectstore').hide();
      jQuery('#sync').show();
    }
  }
  function updateURL(tab) {
    if (history.pushState) {
        var url = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.search;
        var newurl = replaceUrlParam( url, 'tab',tab );
        window.history.pushState({path:newurl},'',newurl);
    }
  }

  function replaceUrlParam(url, paramName, paramValue){
    var pattern = new RegExp('(\\?|\\&)('+paramName+'=).*?(&|$)')
    var newUrl=url
    if(url.search(pattern)>=0){
        newUrl = url.replace(pattern,'$1$2' + paramValue + '$3');
    }
    else{
        newUrl = newUrl + (newUrl.indexOf('?')>0 ? '&' : '?') + paramName + '=' + paramValue
    }
    return newUrl
  }

  // Save setting data
  jQuery("#save").click(function(e){
    jQuery('#save').html('Loading..');
    e.preventDefault();
    var url  = jQuery("#url").val();
    var key1 = jQuery("#key1").val();
    var key2 = jQuery("#key2").val();
    if (url == '') {
      fieldBlankFocus("url");
      jQuery('#save').html('Save');
      return false;
    }
    if (key1 == '') {
      fieldBlankFocus("key1");
      jQuery('#save').html('Save');
      return false;
    }
    if (key2 == '') {
      fieldBlankFocus("key2");
      jQuery('#save').html('Save');
      return false;
    }
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url( __FILE__ ).'sync-function.php?type=save-data'; ?>",
      data: {url:url,key1:key1,key2:key2},
      success: function(msg){
        jQuery('#save').html('Save');
        jQuery('#myAlert').html("<div class='notice notice-success is-dismissible' style='width: 20%'><p><strong>Success!</strong> Setting Saved.</p></div>");
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        alert("some error");
      }
    });
  });
  // GET Stores
  jQuery("#store").click(function(e){
    jQuery('#store').prop('disabled',true).css({'opacity':0.5,'cursor':'not-allowed'});
    jQuery('#products').prop('disabled',true).css({'opacity':0.5,'cursor':'not-allowed'});
    jQuery('#sync-msg').html("");
    jQuery('#gif').show();
    e.preventDefault();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url( __FILE__ ).'sync-function.php?type=sync-stores&sync=manually'; ?>",
      success: function(msg){
        jQuery('#store').prop('disabled',false).removeAttr('style');
        jQuery('#products').prop('disabled',false).removeAttr('style');
        jQuery('#gif').hide();
        if (msg == 'success') {
          jQuery('#sync-msg').html("<h3 style='color:green;'>Store Sync Complete</h3>");
        }else{
          jQuery('#sync-msg').html("<h3 style='color:red;'>"+msg+"</h3>");
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        jQuery('#store').prop('disabled',false).removeAttr('style');
        jQuery('#products').prop('disabled',false).removeAttr('style');
        jQuery('#gif').hide();
        jQuery('#sync-msg').html("<h3 style='color:red;'>"+errorThrown+"</h3>");
      }
    });
  });
  // GET Store Product Data
  jQuery("#products").click(function(e){
    jQuery('#store').prop('disabled',true).css({'opacity':0.5,'cursor':'not-allowed'});
    jQuery('#products').prop('disabled',true).css({'opacity':0.5,'cursor':'not-allowed'});

    jQuery('#sync-msg').html("");
    jQuery('#gif').show();
    e.preventDefault();
    jQuery.ajax({
      type: "POST",
      url: "<?php echo plugin_dir_url( __FILE__ ).'sync-function.php?type=sync-stores-data&sync=manually'; ?>",
      success: function(msg){
        jQuery('#store').prop('disabled',false).removeAttr('style');
        jQuery('#products').prop('disabled',false).removeAttr('style');
        jQuery('#gif').hide();
        if (msg == 'success') {
          jQuery('#sync-msg').html("<h3 style='color:green;'>Store Product Data Sync Complete</h3>");
        }else{
          jQuery('#sync-msg').html("<h3 style='color:red;'>"+msg+"</h3>");
        }
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        jQuery('#store').prop('disabled',false).removeAttr('style');
        jQuery('#products').prop('disabled',false).removeAttr('style');

        jQuery('#gif').hide();
        jQuery('#sync-msg').html("<h3 style='color:red;'>"+errorThrown+"</h3>");
      }
    });
  });
</script>