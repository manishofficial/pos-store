<?php
// EDIT EMAIL TEMPLATE
add_action( 'woocommerce_email_customer_details', 'store_mail_template_edit', 10 , 4);

function store_mail_template_edit($order, $sent_to_admin, $plain_text, $email){
    $collection_check = $order->get_meta('_shipping_pickup_pos_stores');
    if ($collection_check != '' ) {
         $mailer = WC()->mailer();
        remove_action( 'woocommerce_email_customer_details', array( $mailer, 'customer_details' ), 10, 4 );
        remove_action( 'woocommerce_email_customer_details', array( $mailer, 'email_addresses' ), 20, 4 );

        global $wpdb;
        $table_name = $wpdb->prefix . 'POS_Stores';
        $results = $wpdb->get_results("SELECT * FROM $table_name WHERE name='".$collection_check."'");
        echo '<h2>Please Collect Your Order From "'.$results[0]->name.'" Store.</h2>';
        echo "<address style='padding:12px;color:#636363;border:1px solid #e5e5e5'>";
        if (!empty($results[0]->name)) {
            echo ucfirst($results[0]->name)."<br>";
        }
        if (!empty($results[0]->city)) {
            echo ucfirst($results[0]->city).", ";
        }
        if (!empty($results[0]->landmark)) {
            echo ucfirst($results[0]->landmark).", ";
        }
        if (!empty($results[0]->zip_code)) {
            echo ucfirst($results[0]->zip_code)."<br>";
        }
        if (!empty($results[0]->state)) {
            echo ucfirst($results[0]->state).", ";
        }
        if (!empty($results[0]->country)) {
            echo ucfirst($results[0]->country)."<br>";
        }
        if (!empty($results[0]->mobile)) {
            if (!empty($results[0]->alternate_number)) {
                $contactNo = $results[0]->mobile.", ".$results[0]->alternate_number;
            }else{
                $contactNo = $results[0]->mobile;
            }
        }elseif (!empty($results[0]->alternate_number)) {
           $contactNo = $results[0]->alternate_number;
        }
        echo "<br>";
        echo "For Information contact store : ".$contactNo;
        echo "</address>";
        echo "<br>";
    }
}

// EDIT HEADDER FOR SEND EMAIL TO STORES
add_filter( 'woocommerce_email_headers', 'store_custom_header_add', 10, 3);

function store_custom_header_add( $headers, $object, $order ) {
    global $wpdb;

    $order = new WC_Order($order_id);

    $storeEmail = array();
    $pickStore = $order->get_meta('_shipping_pickup_pos_stores');
    if ($pickStore != '') {
      $table = $wpdb->prefix . 'POS_Stores';
      $get_data = $wpdb->get_results("SELECT pos_id FROM $table WHERE name='$pickStore'");
      $store_location_id = $get_data[0]->pos_id;
      array_push($storeEmail, (int)$store_location_id);
    }

    $items = $order->get_items();
    foreach ( $items as $item ) {
      $store = json_decode($item->get_meta('Store'));
      $storeID = $store[2];
      array_push($storeEmail, (int)$storeID);
    }
    
    $finalEmails = json_encode(array('stores_ids'=>$storeEmail));

    $table_save_data = $wpdb->prefix . 'POS_Stores_sync_setting';
    $get_data = $wpdb->get_results("SELECT * FROM $table_save_data");
    // GET THE KEYS FROM DATABASE FOR AUTH
    if (!empty($get_data)) {
      $url = $get_data[0]->url;
      $key1 = $get_data[0]->key1;
      $key2 = $get_data[0]->key2;

      $auth = base64_encode($key1.":".$key2);
    }else{
      $url = '';
      $key1 = '';
      $key2 = '';
    }

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url."/woocommerce/get_emails",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $finalEmails,
      CURLOPT_HTTPHEADER => array(
        "Accept: */*",
        "Accept-Encoding: gzip, deflate",
        "Authorization: Basic ".$auth,
        "Content-Type: application/json",
      ),
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    if ($err) {
      store_error_log("error","cURL Error #:" . $err);
    } else {
      $response = json_decode($response);
      if ($response->status == 200) {
        $response = $response->data->emails;
        foreach ($response as $email) {
          // Add Cc to headers
          $formatted_email = utf8_decode(' <' . $email . '>');
          $headers .= 'Cc: '.$formatted_email .'\r\n';
          // $headers .= 'Cc: <'.$email .'>\r\n';
        }
      }else{
        store_error_log("error","faild to get email from ".$url);
      }
    }

    // $headers .= 'Cc: '.utf8_decode('<nancyseth7908@gmail.com>') .'\r\n';
    // $headers .= 'Cc: '.utf8_decode('<vikass185@gmail.com>') .'\r\n';
    // $headers .= 'Cc: '.utf8_decode('<manishmalhotraproduction@gmail.com>') .'\r\n';

    return $headers;
}