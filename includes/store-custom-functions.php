<?php
error_reporting(1);
//Woocommerce Checkout hook for check quantity in CSV
include_once 'order-sync/checkout-quantity.php';
// NEW ORDER DECREASE QUANTITY
include_once 'order-sync/new-order.php';
// Order Sync
include_once 'order-sync/order-sync.php';
// Cancel Order Sync
include_once 'order-sync/order-cancel.php';
// SHIPPING METHOD SETTINGS
// include_once 'shipping/shipping-settings.php';
// INCLUDING THE FILE FOR EXTRA FEILD
// include_once 'extra-fields/extra-field.php';
// STORE EMAIL
include_once 'mail/store-mail.php';
// ADD TO CART FUNCTIONALITY
include_once 'add-to-cart/add-to-cart.php';

// HIDE MANAGE STOCK FEILD FROM ADMIN SIDE
add_action('admin_head',function(){
  echo '<style>
  ._manage_stock_field{
    display: none !important; 
  }
  </style>';
});
