<?php

/**
* Displays the custom text field input field in the WooCommerce product data meta box
*/
function product_pos_custom_fields() {
	// Checkbox
	woocommerce_wp_checkbox( 
		array( 
		'id'            => 'Sync_to_pos', 
		'wrapper_class' => 'show_if_simple', 
		'label'         => __('Sync to POS : ', 'woocommerce' ), 
		// 'description'   => __( 'Check me!', 'woocommerce' ),
		'custom_attributes' =>  array('checked' => 'checked')
		)
	);
}
/**
* Saves the custom field data to product meta data
*/
// Check Sync to POS
function save_pos_store_meta( $post_id ) {
	$product = wc_get_product( $post_id );
	$POS_cost = isset( $_POST['Sync_to_pos'] ) ? $_POST['Sync_to_pos'] : '';
	$product->update_meta_data( 'Sync_to_pos', $POS_cost );
	$product->save();
}
add_action( 'woocommerce_process_product_meta', 'save_pos_store_meta' );

add_action( 'add_meta_boxes', 'pos_store_meta' );
function pos_store_meta()
{
    add_meta_box( 'pos-store-meta-box', 'POS Store Setting', 'product_pos_custom_fields', 'product', 'side', 'high' );
}

function pos_store_product_cost_field() {
	$args = array(
		'id' => 'pos_cost_price',
		'label' => __( 'POS Unit Cost', 'cfwc' ),
		'class' => 'cfwc-custom-field',
		'desc_tip' => true,
		'description' => __( 'Enter cost that will be sync to POS.', 'ctwc' ),
		);
	woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_general_product_data', 'pos_store_product_cost_field' );

function cfwc_save_custom_field( $post_id ) {
	$product = wc_get_product( $post_id );

	$POS_cost = isset( $_POST['pos_cost_price'] ) ? $_POST['pos_cost_price'] : '';
	$product->update_meta_data( 'pos_cost_price', $POS_cost );
	$product->save();
}
add_action( 'woocommerce_process_product_meta', 'cfwc_save_custom_field' );
