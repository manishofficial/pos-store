<?php
set_time_limit(0);
$root = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
require_once $root.'/wp-load.php' ;
error_reporting(1);
global $wpdb;
$table_save_data = $wpdb->prefix . 'POS_Stores_sync_setting';
$get_data = $wpdb->get_results("SELECT * FROM $table_save_data");
// GET THE KEYS FROM DATABASE FOR AUTH
if (!empty($get_data)) {
  $url = $get_data[0]->url;
  $key1 = $get_data[0]->key1;
  $key2 = $get_data[0]->key2;

  $auth = base64_encode($key1.":".$key2);
}else{
  $url = '';
  $key1 = '';
  $key2 = '';
}

$SyncType = $_REQUEST['sync'];
// CHECKING THE REQUEST
if ($_REQUEST['type']=='save-data') {
  save_data($table_save_data);
  die();
}elseif ($_REQUEST['type']=='save-data-store') {
  save_default_data($table_save_data);
  die();
}elseif ($_REQUEST['type']=='sync-stores') {
  store_error_log("message",$SyncType." store sync start at ".date("d-m-Y H:i A"));
  sync_stores($url,$auth);
  store_error_log("message",$SyncType." store sync successfully end at ".date("d-m-Y H:i A"));
}elseif ($_REQUEST['type']=='sync-stores-data') {
  store_error_log("message",$SyncType." store data sync start at ".date("d-m-Y H:i A"));
  sync_products_data($url,$auth);
  store_error_log("message",$SyncType." store data sync successfully end at ".date("d-m-Y H:i A"));
}

// THIS FUNCTION USE FOR SAVE KEYS
function save_data($table_save_data){
  global $wpdb;
  $url  = rtrim($_REQUEST['url'],"/");
  $key1 = $_REQUEST['key1'];
  $key2 = $_REQUEST['key2'];

  $check_data_exists = $wpdb->get_results("SELECT * FROM $table_save_data");
  if (empty($check_data_exists)) {
    $wpdb->insert(
        $table_save_data,array(
            'url'   => $url,
            'key1'  => $key1,
            'key2'  => $key2,
        ));
  }else{
    $where = array('id' => $check_data_exists[0]->id);
    $wpdb->update(
        $table_save_data,array(
          'url'   => $url,
          'key1'  => $key1,
          'key2'  => $key2,
        ),$where
    );
  }
}

// THIS FUNCTION USE FOR SYNC STORES
function sync_stores($url,$auth){
  global $wpdb;

  $table_name = $wpdb->prefix . 'POS_Stores';

  $curl = curl_init();
  curl_setopt_array($curl, array(
   CURLOPT_URL => $url."/woocommerce/posStores",
   CURLOPT_RETURNTRANSFER => true,
   CURLOPT_RETURNTRANSFER => true,
   CURLOPT_ENCODING => "",
   CURLOPT_MAXREDIRS => -1,
   CURLOPT_TIMEOUT => 0,
   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
   CURLOPT_CUSTOMREQUEST => "GET",
   CURLOPT_HTTPHEADER => array(
    "Accept: */*",
    "Accept-Encoding: gzip, deflate",
     "Accept-Encoding: gzip, deflate",
     "Authorization: Basic ".$auth,
   ),
  ));
  $response = curl_exec($curl);
  $err = curl_error($curl);
  curl_close($curl);

  if ($err) {
    store_error_log("error","cURL Error #:" . $err);
    echo "cURL Error #:" . $err . " Please check your api settings.";
    die();
  } else {
    $response = json_decode($response);
  }

  if ($response->status == 200) {
    $response = $response->data;
    foreach ($response as $store) {
      if ( $store->name != 'Headoffice' ) {
        $check_store_exists = $wpdb->get_results("SELECT * FROM $table_name WHERE location_id='".$store->location_id."'");

        if (!file_exists(dirname( plugin_dir_path(__FILE__) ) . '/uploads/'.$store->name.'.csv')) {
          $csv_handler = fopen (dirname( plugin_dir_path(__FILE__) ) . '/uploads/'.$store->name.'.csv','w');
          if ($csv_handler == true) {
            $file = dirname( plugin_dir_path(__FILE__) ) . '/uploads/'.$store->name.'.csv';
          }else{
            $file = 'unable to create file.';
            store_error_log("error","store file not exists & ".$file);
          }
          fclose ($csv_handler);
        }else{
          $file = dirname( plugin_dir_path(__FILE__) ) . '/uploads/'.$store->name.'.csv';
        }

        chmod($file, 0777);

        if (empty($check_store_exists)) {
          $wpdb->insert(
            $table_name,array(
              'pos_id'          => $store->id,
              'location_id'     => $store->location_id,
              'name'            => $store->name,
              'country'         => $store->country,
              'state'           => $store->state,
              'landmark'        => $store->landmark,              
              'city'            => $store->city,
              'zip_code'        => $store->zip_code,
              'mobile'          => $store->mobile,
              'alternate_number'=> $store->alternate_number,
              'email'           => $store->email,
              'file'            => $file,
            )
          );
        }else{
            $where = array('location_id' => $store->location_id);
            $wpdb->update(
                $table_name,array(
                  'pos_id'          => $store->id,
                  'location_id'     => $store->location_id,
                  'name'            => $store->name,
                  'country'         => $store->country,
                  'state'           => $store->state,
                  'landmark'        => $store->landmark,              
                  'city'            => $store->city,
                  'zip_code'        => $store->zip_code,
                  'mobile'          => $store->mobile,
                  'alternate_number'=> $store->alternate_number,
                  'email'           => $store->email,
                  'file'            => $file,
                ),$where
            );
        }
      }
    }
    if (!file_exists(dirname( plugin_dir_path(__FILE__) ) . '/uploads/allinonestore.csv')) {
      $csv_handler = fopen (dirname( plugin_dir_path(__FILE__) ) . '/uploads/allinonestore.csv','w');
      if ($csv_handler == true) {
        $file = dirname( plugin_dir_path(__FILE__) ) . '/uploads/allinonestore.csv';
      }else{
        $file = 'unable to create file.';
        store_error_log("error","store file not exists & ".$file);
      }
      chmod($file, 0777);
      fclose ($csv_handler);
    }
    echo "success";
  }else{
    echo $response = json_encode($response);
    store_error_log("error",$response);
  }
}

// THIS FUNCTION USE FOR SYNC STORES PRODUCTS DATA
function sync_products_data($url,$auth){
  global $wpdb;
  $table_save_data = $wpdb->prefix . 'POS_Stores';
  $get_stores = $wpdb->get_results("SELECT * FROM $table_save_data");
  if (!empty($get_stores)) {
    $haveStock = array();
    $notHaveStock = array();
    $loop = -1;
    foreach ($get_stores as $store) {
      $loop++;
      $storeID = $store->pos_id;
      $storeName = $store->name;
      $file = $store->file;
      chmod($file, 0777); 
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_URL => $url."/woocommerce/storeData/".$storeID,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => -1,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
          "Accept: */*",
          "Authorization: Basic ".$auth,
          "Cache-Control: no-cache",
          "Connection: keep-alive",
        ),
      ));

      $response = curl_exec($curl);
      $err = curl_error($curl);

      curl_close($curl);

      if ($err) {
        store_error_log("error","cURL Error #:" . $err);
        echo "cURL Error #:" . $err . " Please check your api settings.";
        die();
      } else {
        $response = json_decode($response);
      }
      if ($response->status == 200) {
        $response = $response->data;
        $tableRow = array_shift(array_slice($response, 0, 1));
        $tableRow = array_keys((array)$tableRow);
        $tableRow = implode(',', $tableRow);

        $csv = $tableRow."\n";//Column headers
        $response = array_map("unserialize", array_unique(array_map("serialize", $response)));
        foreach ($response as $record){
          if ($loop == 0) {
            if ( $record->qty_available > 0 ) {
              if ($record->qty_available <= 5) {
                $stockStatus = "Low Stock";
              }else{
                $stockStatus = "In Stock";
              }
              
              array_push($haveStock,
                array(
                  $record->sku,
                  $record->qty_available,
                  $storeID,
                  $storeName,
                  $stockStatus
                )
              );
            }else{
              $notHaveStock[] = $record->sku;
            }
          }else{
            if (false !== $pos = array_search($record->sku,$notHaveStock)) {
              if ( $record->qty_available > 0 ) {
                unset($notHaveStock[$pos]);
                if ($record->qty_available <= 5) {
                  $stockStatus = "Low Stock";
                }else{
                  $stockStatus = "In Stock";
                }
                array_push($haveStock,
                  array(
                    $record->sku,
                    $record->qty_available,
                    $storeID,
                    $storeName,
                    $stockStatus
                  )
                );
              }
            }
          }
          $csv.= $record->sku.','.(float)$record->qty_available."\n"; //Append data to csv
        }
        chmod($file, 0777);
        $csv_handler = fopen ($file,'w');
        if (!$csv_handler) {
          store_error_log("error","unable to open csv");
          die('unable to open csv');
        }
        fwrite ($csv_handler,$csv);
        fclose ($csv_handler);
      }else{
        echo json_encode($response);
        die();
      }
    }
    //CSV IMPORT UNIQUE CSV FOR STOKE
      chmod(dirname(plugin_dir_path(__FILE__)).'/uploads/allinonestore.csv', 0777); 
      $unique_csv_handler = fopen (dirname(plugin_dir_path(__FILE__)).'/uploads/allinonestore.csv','w');
      if (!$unique_csv_handler) {
        store_error_log("error","unable to open csv");
        die('unable to open csv');
      }
      $requiredHeaders = array("sku","qty_available","storeID","storeName","stockStatus");
      array_unshift($haveStock,$requiredHeaders);
      foreach ($haveStock as $line) {
        fputcsv($unique_csv_handler, $line);
      }
      fclose ($unique_csv_handler);
    //IMPORT STOP
    echo "success";
  }else{
    echo "Store Not Found! Please Sync Store First";
    store_error_log("error","Store Not Found! Please Sync Store First");
    die();
  }
}
