<?php
//SHOW STORES STOCK AT PRODUCT DETAIL PAGE
add_action('woocommerce_before_add_to_cart_button', 'store_addtocart_function');

function store_addtocart_function(){
	global $product,$wpdb;
	$table_save_data = $wpdb->prefix . 'POS_Stores';
	$get_stores = $wpdb->get_results("SELECT * FROM $table_save_data");

    $sku = $product->get_sku();
    $productType = $product->get_type();
    echo "<input type='hidden' id='POS_Stores_product_type' value='".$productType."'>";
    $imgDir = dirname(dirname(plugin_dir_url( __FILE__ )));
    if ($product->is_in_stock()) {
	    if ($productType == 'bundle') {
	    	$bundle = new WC_Product_Bundle($product->get_id());
		    $Items = $bundle->get_bundled_items();
		    $loop = -1;
		    $itemStatus = array();
		    foreach ($Items as $item) {
		    	$loop++;
		    	$itemSku = $item->product->sku;
		    	if ( $loop == 0 ) {
		    		$storeData = array();
					foreach ($get_stores as $store) {
					    $storeID = $store->pos_id;
					    $storeName = $store->name;
					    $file = $store->file;
					    if (file_exists($file)) {
					    	$fileHandle = fopen($file, "r");
							// Loop through the CSV rows.
							while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
							    //Print out my column data.
							    if ($row[0]==$itemSku) {
							    	if($row[1] <= 0){
							    		$row[1] = 0;
							    	}
							    	array_push($storeData, array('name'=>$storeName,'store_id'=>$storeID , 'sku'=>$row[0], 'qty'=>$row[1]));
							    }
							}
					    }
					}
		    	}
		    	$file_link = dirname(dirname( plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv';
		        if (file_exists($file_link)) {
		            $fileHandle = fopen($file_link, "r");
		            // Loop through the CSV rows.
		            while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
		                //Print out my column data.
		                if ($row[0]==$itemSku) {
		                    if($row[1] > 0){
		                    	$stockStatus = "in_stock";
		                    }else{
		                    	$stockStatus = "out_of_stock";
		                    }
		                    // array_push($itemStatus, array('sku'=>$row[0],'status'=>$stockStatus));
		                    break;
		                }else{
		                	$stockStatus = "out_of_stock";
		                }
		            }
		        }
		        if($stockStatus == "out_of_stock"){
		        	break;
		        }
		    }
		    echo "<input type='hidden' name='POS_Stores_Status' id='POS_Stores_Status' value='".$stockStatus."' >";
	    }elseif ($productType == 'variable') {
	    	echo "<input type='hidden' name='variable_reset' id='variable_reset' value='no' >";
	    	$product = wc_get_product($product->get_id());

			// Get children product variation IDs in an array
			$children_ids = $product->get_children();
			$variationData = array();
			foreach ($children_ids as $children_id) {
				$product = wc_get_product($children_id);
				$sku = $product->get_sku();
				$varStoreData = array();
				foreach ($get_stores as $store) {
				    $storeID = $store->pos_id;
				    $storeName = $store->name;
				    $file = $store->file;

				    if (file_exists($file)) {
				    	$fileHandle = fopen($file, "r");
						// Loop through the CSV rows.
						while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
						    //Print out my column data.
						    if ($row[0]==$sku) {
						    	if($row[1] <= 0){
						    		$row[1] = 0;
						    	}
						    	array_push($varStoreData, array('name'=>$storeName,'store_id'=>$storeID , 'sku'=>$row[0], 'qty'=>$row[1]));
						    }
						}
				    }
				}
				$variationData[$sku] = $varStoreData;
			}
	    }else{
			$storeData = array();
			foreach ($get_stores as $store) {
			    $storeID = $store->pos_id;
			    $storeName = $store->name;
			    $file = $store->file;

			    if (file_exists($file)) {
			    	$fileHandle = fopen($file, "r");
					// Loop through the CSV rows.
					while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
					    //Print out my column data.
					    if ($row[0]==$sku) {
					    	if($row[1] <= 0){
					    		$row[1] = 0;
					    	}
					    	array_push($storeData, array('name'=>$storeName,'store_id'=>$storeID , 'sku'=>$row[0], 'qty'=>$row[1]));
					    }
					}
			    }
			}
		}
		if ($productType != 'variable') {
			if ($stockStatus != 'out_of_stock') {
				foreach ($storeData as $value) {
					if ( $value['qty'] > 0 ) {
						$selectedStore = $value['name'];
						break;
					}
				}
			}else{
				$selectedStore = '';
				$storeData = array();
			}
			
			if ($selectedStore == null) {
				echo "<p class='stock out-of-stock'>Out of stock</p>";
			}
		}else{
			echo "<input type='hidden' name='POS_variable_stores' id='POS-variable-store-data' value='".json_encode($variationData)."'>";
			echo "<p class='stock out-of-stock' style='display:none'>Out of stock</p>";
		} ?>
		<style>
		   .single_add_to_cart_button,.quantity{
		      display: none;
		   }
		   .product-shop-box.col-md-6{
		      width: 100%;
		      max-width: 100%;
		      padding: 1rem 1rem;
		      border: 1px solid #ebebeb;
		      display: inline-block;
		      position: relative;
		      margin-bottom: 1rem;
		   }
		   .product-shop-box.col-md-6 {
		      background: #f9f9f9;
		      margin-bottom: 0.5rem;
		   }
		   .inner-title b {
		       font-size: 13px!important;
		       color: #9b948a!important;
		       font-weight: 500!important;
		   }
		   .product-shop-box .inner-title{
		      text-align: center;
		      margin-bottom: 1rem;
		      color: #333;
		      font-size: 17px;
		   }
		   .product-shop-box .select-box{
		      position: relative;
		      max-width: 55%;
		      margin: auto;
		   }
		   .product-shop-box select{
		       display: block;
		       font-size: 11px;
		       color: #555;
		       vertical-align: middle;
		       background-color: #fff;
		       background-image: none;
		       -webkit-appearance: none;
		       height: 38px;
		       position: relative;
		       border-radius: 0;
		       border: 1px solid #ebebeb;
		       text-transform: uppercase;
		       overflow: hidden !important;
		       width: 100% !important;
		       padding: 3px 16px !important;
		       white-space: initial !important;
		       text-overflow: clip !important;
		   }
		   .product-shop-box select + i.fa{
		      position: absolute;
		       pointer-events: none;
		       background-color: #f6821f;
		       top: 50%;
		       right: .40%;
		       transform: translate(1%,-50%);
		       height: 36px;
		       width: 36px;
		       text-align: center;
		       line-height: 36px;
		       color: #fff;
		       font-size: 13px;
		       font-weight: 100;
		   }
		   .product-shop-box .grid-box{
		      display: grid;
		      grid-template-columns: 1fr 1fr 1fr;
		      grid-column-gap: 10px;
		      padding: 1rem 0;
		   }
		   .product-shop-box .grid-img-box{
		      width: 100%;
		      padding: 1rem 0rem;
		      text-align: center;
		   }
		   .product-shop-box .grid-img-box img{
		      height: 40px;
		      display: block;
		   }
		   .product-shop-box .grid-img-box div{
		      display: flex;
		      justify-content: center;
		   }
		   .product-shop-box .grid-img-box p{
		      margin: 0;
		       font-size: 13px;
		       font-weight: 400;
		       color: #666666;
		       padding-top: 3px;
		   }
		   @media screen and (max-width: 991px){
		      .product-shop-box .select-box{
		             max-width: 74%;
		      }
		   }
		   @media screen and (max-width: 480px){
		      .store-main-box .row{
		         grid-template-columns:auto;

		      }
		      #store-p2{
		         position: inherit;
		         right: 0;
		      }
		      .product-shop-box .select-box{
		         max-width: 100%;
		      }
		      .product-shop-box .grid-img-box img {
		          height: 25px;
		      }
		      .product-shop-box .grid-img-box p {
		          margin: 0;
		          font-size: 11px
		      }
		      .product-shop-box.col-md-6 {
		          padding: 20px 10px;
		      }
		   }
		</style>
		<div class="product-shop-box col-md-6" id="store-list">
			<div class="row">
				<div class="col-md-12">
					<div class="inner-title">
						<b>Store & Stock Availability</b>
					</div>
					<div class="select-box" id="changeStores">
						<select class="form-control" id="select-store" name="select-store" required>
							<option value="">Select your closest store</option>
							<?php if (!empty($storeData)) { ?>
								<?php foreach ($storeData as $data) {
									if ($data['qty'] <= 0) {
										$Qty = "No Stock";
									}elseif ($data['qty'] <=5) {
										$Qty = "Low Stock";
									}else{
										$Qty = "In Stock";
									}
									?>
				        			<option value="<?php echo $data['name'].'|'.$data['qty'].'|'.$data['store_id'] ?>" <?php if ($selectedStore==$data['name']) { echo "selected"; } ?>><p><?php echo $data['name']." : ".$Qty; ?></p></option>
				        		<?php }
				        	} ?>

						</select>
						<i class="fa fa-chevron-down" aria-hidden="true"></i>
					</div>
				</div>
				<div class="col-md-12">
					<div class="grid-box">
						<div class="grid-img-box">
							<div><img id="delivery" src="<?php echo $imgDir.'/assets/images/delivery.png'; ?>"></div>
							<p id="delivery-text">Delivery available</p>
						</div>
						<div class="grid-img-box">
							<div><img id="checkout" src="<?php echo $imgDir.'/assets/images/click-and-collect-default.png'; ?>"></div>
							<p id="checkout-text">Store Collection Coming Soon</p>
							<!-- Click & collct( on checkout) -->
						</div>
						<div class="grid-img-box">
							<div><img id="status" src="<?php echo $imgDir.'/assets/images/small-business-gray.png'; ?>"></div>
							<p id="status-text">In-store-stock unavailable</p>
						</div>
					</div>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			// IF VARIABLE PRODUCT SKU CHANEGE
		    jQuery("body").on('DOMSubtreeModified', ".sku", function() {
		    	jQuery('.single_add_to_cart_button,.quantity').hide();
		  		jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business-gray.png'; ?>");
		  		jQuery('#status-text').html('In-store-stock unavailable');
		  		jQuery(".qty").attr("max",0);
		  		jQuery('.out-of-stock').hide();
		    	if (jQuery('.sku').html() != '') {
		    		if (jQuery('#variable_reset').val() == 'no') {
			    		jQuery(".quantity").css("display",'none !important;');
			    		var sku = jQuery('.sku').html();
			    		var storeData = jQuery('#POS-variable-store-data').val();
			    		storeData = JSON.parse(storeData);
			    		var store = storeData[sku];
			    		var variable_ID = jQuery('.variations_form').data('product_id');
			    		jQuery('input[name=add-to-cart]').val(variable_ID);
			    		jQuery('input[name=product_id]').val(variable_ID);
			    		jQuery.ajax({
					      type: "POST",
					      url: "<?php echo plugin_dir_url( __FILE__ ).'ajax-store-selection.php'; ?>",
					      data: {store:store},
					      success: function(data){
					        jQuery('#select-store').html(data);

					        if (jQuery("#select-store").find(":selected").text().indexOf('Choose an option') != -1) {
					    		jQuery('.single_add_to_cart_button,.quantity').hide();
					    	}else{
					    		jQuery('.single_add_to_cart_button,.quantity').show();
					    		var data = jQuery('#select-store').val().split('|');
					    		var loc = data[0];
							  	var qty = parseInt(data[1]);
							  	var loc_id = data[2];
							  	jQuery(".qty").attr("max",qty);
					    		if (qty > 0 || qty == null) {
						    		jQuery('#stock-status').html("<p style='color:#4caf50;'>In Stock</p>");
						  			jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business.png'; ?>");
						  			jQuery('#status-text').html('In-store-stock');
						  		}else{
						  			jQuery('.single_add_to_cart_button,.quantity').hide();
							  		jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business-gray.png'; ?>");
							  		jQuery('#status-text').html('In-store-stock unavailable');
							  		jQuery('.out-of-stock').show();
							  		console.log(jQuery('.reset_variations').css("visibility"));
							  		if (jQuery('.reset_variations').css("visibility") == "hidden") {
										jQuery('.out-of-stock').hide();				  			
							  		}
						  		}
					    	}
					      },
					      error: function(XMLHttpRequest, textStatus, errorThrown) {
					        alert("some error");
					      }
					    });
					}else{
						jQuery('#select-store').html('<option value="">Select your closest store</option>');
					}
		    	}
			});

		    // IF VARIABLE PRODUCT VARIATION CHANEGE
		    jQuery( ".variations_form" ).on( "woocommerce_variation_select_change", function () {
		    	if (jQuery(this).find(":selected").text().indexOf('Choose an option') != -1) {
		    		jQuery('#variable_reset').val('yes');
		    		jQuery('.single_add_to_cart_button,.quantity').hide();
		    	}else{
		    		jQuery('#variable_reset').val('no');
		    	}
			} );

			jQuery('#select-store').on('change', function() {
				var value = this.value.split('|');
				var location = value[0];
				var qty = value[1];
				var comboCheck = jQuery('#POS_Stores_Status').val();
				if (comboCheck != 'out_of_stock') {
					if(qty <= 0 || qty==null){
						jQuery('.single_add_to_cart_button,.quantity').hide();
						jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business-gray.png'; ?>");
				  		jQuery('#status-text').html('In-store-stock unavailable');
					}else{
						jQuery('.single_add_to_cart_button,.quantity').show();
						if ( jQuery('input[name="combo-detail[]"]').length ) {
							var data = jQuery('#select-store').val().split('|');
							var loc = data[0];
						  	var qty = parseInt(data[1]);
						  	var loc_id = data[2];
						  	var values = [];
						  	let qtyValue;
							  	jQuery("input[name='combo-detail[]']").each(function(i) {
									if (i==0) {
										jQuery(this).attr('data-store_id',loc_id);
										jQuery(this).attr('data-store_name',loc);
										jQuery(this).attr('data-qty',qty); 
										qtyValue = qty;
									}else{
										if (parseInt(jQuery(this).data('qty')) < qty) {
											qtyValue = parseInt(jQuery(this).data('qty'));
										}
									}
									values.push(qtyValue);
								});
							jQuery(".qty").attr("max",qtyValue);
						}else{
							jQuery(".qty").attr("max",qty);
						}
					}
				}
			  	jQuery('#location').html(location);
			  	jQuery('#qty').html('Available Quantity : '+qty);
			  	
			  	if (qty) {
				  	if ( qty <= 0 ) {
			  			jQuery('#stock-status').html("<p style='color:red;'>No Stock</p>");
			  			jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business-gray.png'; ?>");
			  			jQuery('#status-text').html('In-store-stock unavailable');
				  	}else if(qty <= 5){
				  		jQuery('#stock-status').html("<p style='color:#ffa900;'>Low Stock</p>");
			  			jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business.png'; ?>");
			  			jQuery('#status-text').html('In-store-stock');
				  	}else{
				  		jQuery('#stock-status').html("<p style='color:#4caf50;'>In Stock</p>");
			  			jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business.png'; ?>");
			  			jQuery('#status-text').html('In-store-stock');
				  	}
				}else{
					jQuery('#stock-status').html("<p style='color:red;'>No Stock</p>");
			  		jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business-gray.png'; ?>");
			  		jQuery('#status-text').html('In-store-stock unavailable');
				}
			});

			var getProductType = jQuery('#POS_Stores_product_type').val();
			if (getProductType == 'bundle') {
				var comboCheck = jQuery('#POS_Stores_Status').val();
				var data = jQuery('#select-store').val().split('|');
				var loc = data[0];
			  	var qty = data[1];
			  	var loc_id = data[2];
			  	var values=[];
			  	jQuery("input[name='combo-detail[]']").each(function(i) {
					if (i==0) {
						jQuery(this).attr('data-store_id',loc_id);
						jQuery(this).attr('data-store_name',loc);
						jQuery(this).attr('data-qty',qty);
					}
				});
			}else{
				var comboCheck = 'in_stock';
			}

			if (comboCheck == 'in_stock') {
				if (jQuery('#select-store').length) {
					jQuery('#location').html("Only available Online");
					var data = jQuery('#select-store').val().split('|');
					var loc = data[0];
				  	var qty = data[1];
				  	jQuery(window).load(function(){
				  		if(qty <= 0 || qty == null){
							jQuery('.single_add_to_cart_button,.quantity').hide();
							jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business-gray.png'; ?>");
					  		jQuery('#status-text').html('In-store-stock unavailable');
						}else{
							jQuery('.single_add_to_cart_button,.quantity').show();
							jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business.png'; ?>");
					  		jQuery('#status-text').html('In-store-stock');
					  		if ( jQuery('input[name="combo-detail[]"]').length ) {
								var values = [];
								jQuery("input[name='combo-detail[]']").each(function() {
									if (parseInt(jQuery(this).data('qty')) < qty) {
										qty = parseInt(jQuery(this).data('qty'));
									}
								});
								jQuery(".qty").attr("max",qty);
							}else{
								jQuery(".qty").attr("max",qty);
							}
						}
					});
				  	
				  	jQuery('#location').html(loc);
				}else{
					jQuery(window).load(function(){
						jQuery('.single_add_to_cart_button,.quantity').hide();
						jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business-gray.png'; ?>");
					  	jQuery('#status-text').html('In-store-stock unavailable');
					});
				}
			}else{
				jQuery(window).load(function(){
					jQuery('.single_add_to_cart_button,.quantity').hide();
					jQuery('#location').html("<p style='color:red;'>Product Not Available.</p>");
					jQuery('#status').attr("src","<?php echo $imgDir.'/assets/images/small-business-gray.png'; ?>");
					jQuery('#status-text').html('In-store-stock unavailable');
				});
			}
		</script>
<?php
	}
}