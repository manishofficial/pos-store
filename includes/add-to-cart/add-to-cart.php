<?php

// ADD TO CART FUNCTION
include_once 'loop-add-to-cart.php';

//SHOW STORES STOCK AT PRODUCT DETAIL PAGE
include_once 'product-detail-page.php';


// Add Product to cart hook
add_filter( 'woocommerce_add_cart_item_data', 'POS_add_store_text_to_cart_item', 10, 3 );
function POS_add_store_text_to_cart_item($cart_item_data, $product_id, $variation_id ) {
    global $wpdb;
    $product = wc_get_product( $product_id );
    $sku = $product->get_sku();
    $productType = $product->get_type();
    if ($productType == 'bundle') {
        $bundle = new WC_Product_Bundle($product_id);
        $bundleItems = $bundle->get_bundled_items();
        if (!empty($bundleItems)) {
          $bundleID = $product->get_id();
          foreach ($bundleItems as $bundleItem) {
            $bundleItemSku[] = $bundleItem->product->sku;
          }
        }
        $sku = $bundleItemSku[0];
    }

    $POS_add_to_cart = filter_input( INPUT_POST, 'select-store' );
    if (empty($POS_add_to_cart)) {
        $file_link = dirname(dirname( plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv';
        if (file_exists($file_link)) {
            $fileHandle = fopen($file_link, "r");
            // Loop through the CSV rows.
            while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
                //Print out my column data.
                if ($row[0]==$sku) {
                    if($row[1] > 0){
                        $sku_match = true;
                        $storeQty = $row[1];
                        $storeId = $row[2];
                        $storeName = $row[3];
                    }else{
                        $sku_match = false;
                    }
                }
            }
        }
        $POS_add_to_cart = $storeName."|".$storeQty."|".$storeId;
    }

    $POS_add_to_cart = explode('|', $POS_add_to_cart);
    $POS_add_to_cart = json_encode($POS_add_to_cart);
    if ($productType != 'bundle') {
        $cart_item_data['pos_store_location_ID'] = $POS_add_to_cart;
    }else{
        $cart_item_data['pos_store_bundle_main'] = $POS_add_to_cart;
    }
    return $cart_item_data;
}
//Filter for combo product
add_filter('woocommerce_bundle_container_cart_item','bundle_cart_edit_items',10,2);
function bundle_cart_edit_items($cart_item, $bundle){
    $loop = -1;
    foreach ($cart_item['stamp'] as $key => $item) {
        $loop++;
        if ($loop != 0) {
            $product = wc_get_product( $item['product_id'] );
            $itemSku = $product->get_sku();

            $file_link = dirname(dirname( plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv';
            if (file_exists($file_link)) {
                $fileHandle = fopen($file_link, "r");
                // Loop through the CSV rows.
                while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
                    //Print out my column data.
                    if ($row[0]==$itemSku) {
                            $qty = (int)$row[1];
                            $storeId = $row[2];
                            $storeName = $row[3];
                            break;
                    }
                }
            }
            $POS_add_to_cart = array($storeName,$qty,$storeId);
            $POS_add_to_cart = json_encode($POS_add_to_cart);
            $cart_item['stamp'][$key]['pos_store_bundle_location_ID'] = $POS_add_to_cart;
        }else{
            $cart_item['stamp'][$key]['pos_store_bundle_location_ID'] = $cart_item['pos_store_bundle_main'];
        }
    }
    return $cart_item;
}
//Display store name in cart
add_filter( 'woocommerce_get_item_data', 'POS_display_store_text_cart', 10, 2 );
function POS_display_store_text_cart( $item_data, $cart_item ) {
    if ( empty($cart_item['stamp']) ) {
        if ( empty( $cart_item['pos_store_location_ID'] ) ) {
            return $item_data;
        }
        $vlaue = json_decode($cart_item['pos_store_location_ID']);
        $item_data[] = array(
            'key'     => __( 'Store', 'store' ),
            'value'   => wc_clean( $vlaue[0] ),
            'display' => '',
        );
        echo "<input type='hidden' name='pos-product-maxqty[]' id='pos-product-maxqty' value='".$vlaue[1]."' >";
    }else{
        $product_id = $cart_item['product_id'];
        $bundle = new WC_Product_Bundle($product_id);
        $bundleItems = $bundle->get_bundled_items();
        foreach ($bundleItems as $item) {
            $bundle_id = $item->data->get_bundle_id();
        }
        if ($bundle_id != $product_id) {
            foreach ($cart_item['stamp'] as $key => $value) {
                if ($value['product_id'] == $product_id) {
                    $StoreData = $value['pos_store_bundle_location_ID'];
                    $StoreData = json_decode($StoreData);
                    echo "<input type='hidden' name='pos-product-maxqty[]' id='pos-product-maxqty' value='".$StoreData[1]."' >";
                    $StoreData = $StoreData[0];
                }
            }
            $item_data[] = array(
                'key'     => __( 'Store', 'store' ),
                'value'   => wc_clean( $StoreData ),
                'display' => '',
            );
        }else{
            $vlaue = json_decode($cart_item['pos_store_location_ID']);
            if ($vlaue != '') {
                echo "<input type='hidden' name='pos-product-maxqty[]' id='pos-product-maxqty' value='".$vlaue[1]."' >";
                $item_data[] = array(
                    'key'     => __( 'Store', 'store' ),
                    'value'   => wc_clean( $vlaue[0] ),
                    'display' => '',
                );
            }
        }
    }   
    return $item_data;
}
// CHECKOUT SHOW STORES
add_action( 'woocommerce_checkout_create_order_line_item', 'POS_add_store_text_to_order_items', 10, 4 );
function POS_add_store_text_to_order_items( $item, $cart_item_key, $values, $order ) {
    $storeData = array('pos_store_location_ID'=>$values['pos_store_location_ID']);
    if (!empty($values['stamp'])) {
        $bundleLocations = array();
        foreach ($values['stamp'] as $bundle) {
            $bundleLocations[$bundle['product_id']] = $bundle['pos_store_bundle_location_ID'];
        }
        $storeData['bundle_location_id'] = $bundleLocations;
    }

    if ( empty( $values['pos_store_location_ID'] ) ) {
        return;
    }
    $item->add_meta_data( 'Store', $storeData );
}
// FILTER THE STORE META ON ORDER DETAIL PAGE
add_filter( 'woocommerce_order_item_display_meta_value', 'change_order_item_meta_value', 10, 3 );
function change_order_item_meta_value( $value, $meta, $item ) {

    // $product = $item->get_product();
    // echo $sku = $product->get_sku();
    // echo $item['product_id'];
    // echo "<pre>"; print_r($item->get_meta('Store'));
    // $value = json_decode($value);
    // if (is_array($value)) {
    //     // By using $meta-key we are sure we have the correct one.
    // if ( 'Store' === $meta->key ) { $value = $value[0]; }
    //     return $value;
    // }else{
    //     return $value;
    // }
    return $value;
}
add_action( 'woocommerce_after_order_itemmeta', 'order_meta_customized_display',10, 3 );
 
function order_meta_customized_display( $item_id, $item, $product ){
    if ($product) {
        $productID = $product->get_id();
        $bundleProduct = $item->get_meta('Store');
        foreach ($bundleProduct as $key => $bundleItem) {
            foreach ($bundleItem as $key => $value) {
                if ($productID == $key) {
                    $store = json_decode($value);
                    $storeName = $store[0];
                    break 2;
                }
            }
        }

        if (empty($storeName)) {
            $store = json_decode( $bundleProduct['pos_store_location_ID'] );
            $storeName = $store[0];
        }
        if ($storeName!='') {
            echo "Store : ".$storeName;
        }
    }
}
add_filter( 'woocommerce_admin_html_order_item_class', 'change_order_item_meta_value_bundle', 10, 3 );
function change_order_item_meta_value_bundle( $value, $meta, $item ) {
    // echo "<pre>"; print_r($meta);

}

// UPDATE ORDER META WITH PENDIND STATUS
add_action('woocommerce_checkout_create_order', 'before_checkout_create_order', 20, 2);
function before_checkout_create_order( $order, $data ) {
    $order->update_meta_data( 'store_order_sync_to_pos', 'pending' );
}

// SHOW SYNC STATUS TO ORDER ADMIN PAGE AFTER BILLING
function show_order_sync_status_to_admin($order){
    echo "<p><strong>POS Sync Status:</strong> " . get_post_meta( $order->id, 'store_order_sync_to_pos', true ) . "</p>";
} 
add_action( 'woocommerce_admin_order_data_after_billing_address', 'show_order_sync_status_to_admin', 10, 1 );
// REMOVED WOOCOMMERCE ADD TO CART BEFORE LOOP FUNCTION
add_action( 'woocommerce_before_shop_loop_item', function(){
    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
}, 30 );
// SHOWING BUNDLE PRODUCT STOCK ON PRODUCT DETAIL PAGE. 
add_action('woocommerce_bundled_item_details','woocommerce_bundle_show_stock_status',26,2);
function woocommerce_bundle_show_stock_status($product,$bundle){
    $productId = $product->product_id;
    $productData = wc_get_product( $productId );
    $ItemSku = $productData->get_sku();
    if ($productData->is_in_stock()) {
        $file_link = dirname(dirname( plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv';
        if (file_exists($file_link)) {
            $fileHandle = fopen($file_link, "r");
            // Loop through the CSV rows.
            while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
                //Print out my column data.
                if ($row[0]==$ItemSku) {
                    if($row[1] > 0){
                        $stockStatus = "<p style='color:#008000;'>In Stock</p>";
                    }else{
                        $stockStatus = "<p style='color:red;'>Out of Stock</p>";
                    }
                    $ComboDetail = "<input type='hidden' id='combo-detail".$ItemSku."' name='combo-detail[]' data-sku='".$ItemSku."' data-store_id='".$row[2]."' data-store_name='".$row[3]."' data-qty='".$row[1]."'>";
                    break;
                }else{
                    $stockStatus = "<p style='color:red;'>Out of Stock</p>";
                }
            }
            if ($stockStatus != '') {
                echo $stockStatus.$ComboDetail;
            }else{
                echo $ComboDetail;
            }
        }
    }   
}
// CART PAGE MAX ADD CART ITEMS LIMIT
add_filter( 'woocommerce_cart_item_quantity', 'so_cart_item_quantity', 10, 3 );
function so_cart_item_quantity( $product_quantity, $cart_item_key, $cart_item ){
    $_product = $cart_item['data'];
    $product_id = $_product->get_id();
    if (isset($cart_item['stamp'])) {
        $max_value = 0;
        foreach ($cart_item['stamp'] as $value) {
            $store = $value['pos_store_bundle_location_ID'];
            $store = json_decode($store);
            if ($max_value > $store[1] || $max_value == 0) {
                $max_value = $store[1];
            }
        }
    }else{
        $store = $cart_item['pos_store_location_ID'];
        $store = json_decode($store);
        $max_value = $store[1];
    }
    // set a max of 10 as default if max is null
    $product_quantity = woocommerce_quantity_input( array(
            'input_name'  => "cart[{$cart_item_key}][qty]",
            'input_value' => $cart_item['quantity'],
            'max_value'   => $max_value,
            'min_value'   => '1'
    ), $_product, false );
    return $product_quantity;
}
