<?php
	$storeData = $_POST['store'];
	$selectedStore = '';
	?>
		<option value="">Select your closest store</option>
		<?php if (!empty($storeData)) { ?>
			<?php foreach ($storeData as $data) {
				if ($data['qty'] <= 0) {
					$Qty = "No Stock";
				}elseif ($data['qty'] <=5) {
					$Qty = "Low Stock";
				}else{
					$Qty = "In Stock";
				}
				if ($selectedStore == '') {
					if ($data['qty'] > 0) {
						$selectedStore = $data['name'];
					}
				}
				?>
    			<option value="<?php echo $data['name'].'|'.$data['qty'].'|'.$data['store_id'] ?>" <?php if ($selectedStore==$data['name']) { echo "selected"; } ?>><p><?php echo $data['name']." : ".$Qty; ?></p></option>
    		<?php }
    	} ?>