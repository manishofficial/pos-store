<?php
// Second, add View Product Button
add_action( 'woocommerce_after_shop_loop_item', 'pos_store_view_product_button', 10 );
function pos_store_view_product_button() {
    global $product;
    $productType = $product->get_type();
    $link = $product->get_permalink();
    $cart = $product->add_to_cart_url();
    $productId = $product->get_id();
    $sku = $product->get_sku();
    if (!$product->is_in_stock()) {
    	echo '<a href="'.get_permalink().'" rel="nofollow" class="outstock_button">Out of Stock</a>';
    }else{
	    if ($productType == 'variable') {
	    	 echo '<div class="add-to-cart-button"><a href="'.get_permalink().'" rel="nofollow" class="%s %s product_type_%s button %s is-%s mb-0 is-%s">View Product</a></div>';
	    }elseif ($product->get_type() == 'bundle') {
	        $sku_match = false;
	        $storeId = "";
	        $storeName = "";

	        $bundle = new WC_Product_Bundle($product->get_id());
	        $bundleItems = $bundle->get_bundled_items();
	        if (!empty($bundleItems)) {
	          $bundleID = $product->get_id();
	          foreach ($bundleItems as $bundleItem) {
	            $bundleItemSku[] = $bundleItem->product->sku;
	          }
	        }
	        $file_link = dirname(dirname( plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv';
	        if (file_exists($file_link)) {
	            // Loop through the CSV rows.
	            $loop = -1;
	            $stockArr = array();
	            foreach ($bundleItemSku as $sku) {
	            	$loop++;
	            	$fileHandle = fopen($file_link, "r");
	            	while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
		                if ($row[0] ==$sku) {
		                    if($row[1] > 0){
		                        // $sku_match = true;
		                        $stockArr[$loop] = 1;
		                        $storeId = $row[2];
		                        $storeName = $row[3];
		                    }else{
		                        // $sku_match = false;
		                        $stockArr[$loop] = 0;
		                    }
		                    break;
		                }else{
		                	// $sku_match = false;
		                	$stockArr[$loop] = 0;
		                }
		            }
	            }

	            if (!in_array(0,$stockArr)) {
	                echo "<input type='hidden' name='storeName' value='".$storeName."'>";
	                echo "<input type='hidden' name='storeId' value='".$storeId."'>";
	                echo '<a href="' . $cart . '" class="button add_to_cart_button ajax_add_to_cart" data-product_id="'.$productId .'" data-product_sku="'.$sku.'" data-product_store_name="'.$storeName.'" data-product_store_id="'.$storeId.'">Add to cart</a>';
	            }else{
	                echo '<a href="'.get_permalink().'" rel="nofollow" class="outstock_button">Out of Stock</a>';
	            }
	        }else{
	        	echo '<a href="'.get_permalink().'" rel="nofollow" class="outstock_button">Out of Stock</a>';
	        }
	    }else{
	        global $wpdb;
	        $sku = $product->get_sku();
	        $sku_match = false;
	        $storeId = "";
	        $storeName = "";
	        $file_link = dirname(dirname( plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv';
	        if (file_exists($file_link)) {
	            $fileHandle = fopen($file_link, "r");
	            // Loop through the CSV rows.
	            while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
	                // echo "<pre>"; print_r($row); die();
	                //Print out my column data.
	                if ($row[0]==$sku) {
	                    if($row[1] > 0){
	                        $sku_match = true;
	                        $storeId = $row[2];
	                        $storeName = $row[3];
	                    }else{
	                        $sku_match = false;
	                    }
	                }
	            }
	            if ($sku_match == true) {
	                echo "<input type='hidden' name='storeName' value='".$storeName."'>";
	                echo "<input type='hidden' name='storeId' value='".$storeId."'>";
	                echo '<a href="' . $cart . '" class="button add_to_cart_button ajax_add_to_cart" data-product_id="'.$productId .'" data-product_sku="'.$sku.'" data-product_store_name="'.$storeName.'" data-product_store_id="'.$storeId.'">Add to cart</a>';
	            }else{
	                // echo "<a href='#' class='button' >Choose Store</a>";
	                echo '<a href="'.get_permalink().'" rel="nofollow" class="outstock_button">Out of Stock</a>';
	            }
	        }else{
	        	echo '<a href="'.get_permalink().'" rel="nofollow" class="outstock_button">Out of Stock</a>';
	        }
		}
	}
}

// ADD TO CART MODIFICATION AS PER THEME
add_action( 'init', 'custom_theme_add_to_cart_functionality');

function custom_theme_add_to_cart_functionality() {
    $theme = wp_get_theme();
    if ($theme->name == 'Flatsome Child') {
        //REMOVING CUSTOM THEME AND PLUGIN VIEW BUTTON
        remove_action( 'flatsome_product_box_after', 'flatsome_woocommerce_shop_loop_button', 100 );
        remove_action( 'woocommerce_after_shop_loop_item', 'pos_store_view_product_button');
        remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );

        add_action( 'flatsome_product_box_after', 'flatsome_woocommerce_shop_loop_button_store', 100 );
        if ( ! function_exists( 'flatsome_woocommerce_shop_loop_button_store' ) ) {
            /**
             * Add 'Add to Cart' After
             */
            function flatsome_woocommerce_shop_loop_button_store() {
                global $product;
			    $productType = $product->get_type();
			    $link = $product->get_permalink();
			    $cart = $product->add_to_cart_url();
			    $productId = $product->get_id();
			    $sku = $product->get_sku();
			    if (!$product->is_in_stock()) {
			    	echo '<div class="add-to-cart-button"><a href="'.get_permalink().'" rel="nofollow" class="%s %s product_type_%s button %s is-%s mb-0 is-%s">Out of Stock</a></div>';
			    }else{
				    if ($productType == 'variable') {
				    	echo '<div class="add-to-cart-button"><a href="'.get_permalink().'" rel="nofollow" class="%s %s product_type_%s button %s is-%s mb-0 is-%s">View Product</a></div>';
				    }elseif ($product->get_type() == 'bundle') {
				        $sku_match = false;
				        $storeId = "";
				        $storeName = "";

				        $bundle = new WC_Product_Bundle($product->get_id());
				        $bundleItems = $bundle->get_bundled_items();
				        if (!empty($bundleItems)) {
				          $bundleID = $product->get_id();
				          foreach ($bundleItems as $bundleItem) {
				            $bundleItemSku[] = $bundleItem->product->sku;
				          }
				        }
				        $file_link = dirname(dirname( plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv';
				        if (file_exists($file_link)) {
				            // Loop through the CSV rows.
				            $loop = -1;
	            			$stockArr = array();
				            foreach ($bundleItemSku as $sku) {
				            	$loop++;
				            	$fileHandle = fopen($file_link, "r");
				            	while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
					                if ($row[0] ==$sku) {
					                    if($row[1] > 0){
					                        // $sku_match = true;
					                        $stockArr[$loop] = 1;
					                        $storeId = $row[2];
					                        $storeName = $row[3];
					                    }else{
					                        // $sku_match = false;
					                        $stockArr[$loop] = 0;
					                    }
					                    break;
					                }else{
					                	// $sku_match = false;
					                	$stockArr[$loop] = 0;
					                }
					            }
				            }
				            if (!in_array(0,$stockArr)) {
				                echo "<input type='hidden' name='storeName' value='".$storeName."'>";
                                echo "<input type='hidden' name='storeId' value='".$storeId."'>";
                                echo '<div class="add-to-cart-button"><a href="' . $cart . '" class="button add_to_cart_button ajax_add_to_cart" data-product_id="'.$productId .'" data-product_sku="'.$sku.'" data-product_store_name="'.$storeName.'" data-product_store_id="'.$storeId.'">Add to cart</a></div>';
				            }else{
				                echo '<div class="add-to-cart-button"><a href="'.get_permalink().'" rel="nofollow" class="%s %s product_type_%s button %s is-%s mb-0 is-%s">Out of Stock</a></div>';
				            }
				        }else{
				        	echo '<div class="add-to-cart-button"><a href="'.get_permalink().'" rel="nofollow" class="%s %s product_type_%s button %s is-%s mb-0 is-%s">Out of Stock</a></div>';
				        }
				    }else{
				        global $wpdb;
				        $sku = $product->get_sku();
				        $sku_match = false;
				        $storeId = "";
				        $storeName = "";
				        $file_link = dirname(dirname( plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv';
				        if (file_exists($file_link)) {
				            $fileHandle = fopen($file_link, "r");
				            // Loop through the CSV rows.
				            while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
				                // echo "<pre>"; print_r($row); die();
				                //Print out my column data.
				                if ($row[0]==$sku) {
				                    if($row[1] > 0){
				                        $sku_match = true;
				                        $storeId = $row[2];
				                        $storeName = $row[3];
				                    }else{
				                        $sku_match = false;
				                    }
				                }
				            }
				            if ($sku_match == true) {
				                echo "<input type='hidden' name='storeName' value='".$storeName."'>";
                                echo "<input type='hidden' name='storeId' value='".$storeId."'>";
                                echo '<div class="add-to-cart-button"><a href="' . $cart . '" class="button add_to_cart_button ajax_add_to_cart" data-product_id="'.$productId .'" data-product_sku="'.$sku.'" data-product_store_name="'.$storeName.'" data-product_store_id="'.$storeId.'">Add to cart</a></div>';
				            }else{
				                echo '<div class="add-to-cart-button"><a href="'.get_permalink().'" rel="nofollow" class="%s %s product_type_%s button %s is-%s mb-0 is-%s">Out of Stock</a></div>';
				            }
				        }else{
				        	echo '<div class="add-to-cart-button"><a href="'.get_permalink().'" rel="nofollow" class="%s %s product_type_%s button %s is-%s mb-0 is-%s">Out of Stock</a></div>';
				        }
					}
				}
            }
        }

        add_filter( 'woocommerce_loop_add_to_cart_link', 'flatsome_woocommerce_shop_loop_button_store', 10, 2 );
    }
}

function woo_api_notice(){
    if(!in_array('woocommerce-api-master/woocommerce-api.php', apply_filters('active_plugins', get_option('active_plugins')))){
        return '<div class="notice notice-info is-dismissible">
                <p>POS Store plugin required woocommerce API Plugin for sync data from POS to woocommerce and woocommerce to POS <a href="https://github.com/ammyrai/woocommerce-api/archive/master.zip">click to download</a> or <a href="plugins.php">activate</a>.</p>
                </div>';
    }else{
        return '';
    }
}
