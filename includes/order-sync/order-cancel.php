<?php

add_action( 'woocommerce_order_status_cancelled', 'pos_store_send_cancel_status', 21, 1 );

function pos_store_send_cancel_status( $order_id ){
  global $wpdb;
  $cancel_order = array('orderNo'=>$order_id);
  $json_data = json_encode($cancel_order);
  $table_save_data = $wpdb->prefix . 'POS_Stores_sync_setting';
  $get_data = $wpdb->get_results("SELECT * FROM $table_save_data");
  if (!empty($get_data)) {
    $url = $get_data[0]->url;
    $key1 = $get_data[0]->key1;
    $key2 = $get_data[0]->key2;

    $auth = base64_encode($key1.":".$key2);
  }else{
    $url = '';
    $key1 = '';
    $key2 = '';
  }

  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => $url."/woocommerce/cancel_order",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $json_data,
    CURLOPT_HTTPHEADER => array(
      "Accept: application/json",
      "Authorization: Basic ".$auth,
      "Content-Type: application/json",
    ),
  ));

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    store_error_log("error","cURL Error #:" . $err);
  } else {
    $response = json_decode($response,TRUE);
    if ( $response['status'] == 200 ) {
      $msg = "orderNo : ".$order_id." || Order cancelled successful || ".$response['msg'];
      store_error_log("notice",$msg);
      // sync_store_data();
      do_action('pos_store_daily');
    }
  }
}