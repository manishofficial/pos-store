<?php
	add_action('woocommerce_thankyou', 'create_invoice_for_wc_order', 10, 1);
	function create_invoice_for_wc_order( $order_id ) {
	    if ( ! $order_id )
	        return;
	    // NEW UPDATE STOCK AS PER SKU
	    function get_new_stock($sku){
	    	global $product,$wpdb;
			$table_save_data = $wpdb->prefix . 'POS_Stores';
			$get_stores = $wpdb->get_results("SELECT * FROM $table_save_data");
			foreach ($get_stores as $store) {
			    $storeID = $store->pos_id;
			    $storeName = $store->name;
			    $file = $store->file;

			    if (file_exists($file)) {
			    	$fileHandle = fopen($file, "r");
					// Loop through the CSV rows.
					while (($row = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
					    //Print out my column data.
					    if ($row[0]==$sku) {
					    	if($row[1] > 0){
					    		if ($row[1] <= 0) {
									$stockStatus = "No Stock";
								}elseif ($row[1] <= 5) {
									$stockStatus = "Low Stock";
								}else{
									$stockStatus = "In Stock";
								}
					    		$StoreData = array($row[0],$row[1],$storeID,$storeName,$stockStatus);
					    		break 2;
					    	}
					    }
					}
			    }
			}
			return $StoreData;
	    }
	    // Allow code execution only once 
	    if( ! get_post_meta( $order_id, '_thankyou_action_done', true ) ) {
	    	global $wpdb;
		    $order = new WC_Order( $order_id );
		    $items = $order->get_items();
		    $update = array();
		    $uniqueStock = array();
		    // Checking All Item and Filter the Store name for Creating Array
		    foreach ( $items as $item ) {
				$product  = wc_get_product($item->get_product_id());
				$variation_id = $item->get_variation_id();
				if ( $variation_id != 0 ) {
					$product = wc_get_product($variation_id);
					$sku = $product->get_sku();
				}else{
					$sku = $product->get_sku();
				}
				$productId = $item->get_product_id();
				$quantity = $item->get_quantity();

				$store = $item->get_meta('Store');
				// CHECKING THE BUNDLE, SIMPLE OR VARIABLE STORE ID
				if (isset($store['bundle_location_id'])) {
					$storeName = $store['bundle_location_id'][$item->get_product_id()];
				}else{
					$storeName = $store['pos_store_location_ID'];
				}
				// CHECKING IF STORE NAME NOT FOUND LIKE COMBO MAIN PRODUCT NOT HAVE ANY STORE NAME
		        if (!empty($storeName)) {
		            $store = json_decode($store['pos_store_location_ID']);
					$storeName = $store[0];
					$storeID = $store[2];
					// FILTER ARRAY FOR SINGLE CSV UPDATE
					$update[$storeName][] = array('sku' => $sku,'quantity' => $quantity);
					// FILTER ARRAY FOR ALLINONE CSV
					if ( false !== $pos = array_search($sku,array_column($uniqueStock, 'sku')) ) {
						if ( $uniqueStock[$pos]['storeName'] == $storeName) {
							$quantity = $uniqueStock[$pos]['quantity'] + $quantity;
							$uniqueStock[$pos] = array('sku'=>$sku,'quantity'=>$quantity,'storeID'=>$storeID,'storeName'=>$storeName);
						}else{
							$uniqueStock[$pos] = array('sku'=>$sku,'quantity'=>$quantity,'storeID'=>$storeID,'storeName'=>$storeName);
						}
					}else{
						$uniqueStock[] = array('sku'=>$sku,'quantity'=>$quantity,'storeID'=>$storeID,'storeName'=>$storeName);
					}
		    	}
		    }
		    // UPDATING THE SINGLE STORE FILES STOCKS
		    foreach ($update as $storeName => $data) {
		    	$result = array();
				$names = array_column($data, 'sku');
				$QTYs  = array_column($data, 'quantity');
				$unique_names = array_unique($names);
				foreach ($unique_names as $name){
				    $this_keys = array_keys($names, $name);
				    $qty = array_sum(array_intersect_key($QTYs, array_combine($this_keys, $this_keys)));
				    $result[] = array("sku"=>$name,"quantity"=>$qty);
				}
				$data = $result;
		    	$table_save_data = $wpdb->prefix . 'POS_Stores';
			    $get_stores = $wpdb->get_results( "SELECT * FROM $table_save_data WHERE name = '$storeName'" );
			    $file = $get_stores[0]->file;
				$fileHandle = fopen($file,"r") or die("no file!");
				$newdata = array();
				while (($row = fgetcsv($fileHandle)) !== FALSE) {
					if ( false !== $pos = array_search($row[0],array_column($data, 'sku')) ) {
						$row[1] = $row[1] - $data[$pos]['quantity'];
					}
					array_push($newdata, $row);
				}
				fclose($fileHandle);

				if (!empty($newdata)) {
					unlink($file);
				}

				$csv_handler = fopen (dirname(dirname(plugin_dir_path(__FILE__))) . '/uploads/'.$storeName.'.csv','w');
				if ($csv_handler == true) {
					$file = dirname(dirname(plugin_dir_path(__FILE__))) . '/uploads/'.$storeName.'.csv';
				}else{
					$file = 'unable to create file.';
					store_error_log("error","store file not exists & ".$file);
				}
				foreach ($newdata as $line) {
					fputcsv($csv_handler, $line);
				}
				fclose($csv_handler);
		    }

		    // UPDATEING THE ALLINONE CSV STOCKS
			$file = dirname(dirname(plugin_dir_path(__FILE__))).'/uploads/allinonestore.csv';
			$uniqueStockUpdate = array();
			$fileHandle = fopen($file,"r") or die("no file!");
			while (($row = fgetcsv($fileHandle)) !== FALSE) {
				if ( false !== $pos = array_search($row[0],array_column($uniqueStock, 'sku')) ) {
					if ( $row[3] == $uniqueStock[$pos]['storeName'] ) {
						$row[1] = $row[1] - $uniqueStock[$pos]['quantity'];
						if ($row[1] <= 0) {
							$stockStatus = "No Stock";
						}elseif ($row[1] <= 5) {
							$stockStatus = "Low Stock";
						}else{
							$stockStatus = "In Stock";
						}
						$row[4] = $stockStatus;
						if ($stockStatus == 'No Stock') {
							$row = get_new_stock($row[0]); // IF STORE HAVE 0 STOCK SO CHANGEING THE STORE.
						}
					}
				}
				array_push($uniqueStockUpdate, $row);
			}
			if (!empty($uniqueStockUpdate)) {
				unlink($file);
			}
			fclose($fileHandle);
			$csv_handler = fopen (dirname(dirname(plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv','w');
			if ($csv_handler == true) {
				$file = dirname(dirname(plugin_dir_path(__FILE__))) . '/uploads/allinonestore.csv';
			}else{
				$file = 'unable to create file.';
				store_error_log("error","store file not exists & ".$file);
			}	
			// UPDATING THE CSV
			foreach ($uniqueStockUpdate as $line) {
				fputcsv($csv_handler, $line);
			}
			fclose($csv_handler);
	        // Flag the action as done (to avoid repetitions on reload)
	        $order->update_meta_data( '_thankyou_action_done', true );
	        $order->save();
	    }
	}
