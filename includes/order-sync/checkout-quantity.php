<?php
// add_action( 'woocommerce_before_checkout_form', 'pos_store_custom_checkout', 10 );

add_action('woocommerce_after_checkout_validation', 'pos_store_validate_checkout_quantity', 10, 2);

function pos_store_validate_checkout_quantity($data){
	global $wpdb;
	$loop = -1;
	foreach ( WC()->cart->get_cart() as $cart_item ) {
       	$product = $cart_item['data'];
       	if(!empty($product)){
			$product_id = $product->get_id();
			$sku = $product->get_sku();
			if (!empty($cart_item['stamp'])) {
				$loop++;
				if ($loop > 0) {
					$bundleProductData = $cart_item['stamp'];
					foreach ($bundleProductData as $bundleProduct) {
						if ( $bundleProduct['product_id'] == $product_id ) {
							$storeData = json_decode($bundleProduct['pos_store_bundle_location_ID']);
							$storeName = $storeData[0];
						}
					}
				}else{
					$location = $cart_item['pos_store_location_ID'];
					$location = json_decode($location);
					$storeName = $location[0];
				}
			}else{
				$location = $cart_item['pos_store_location_ID'];
				$location = json_decode($location);
				$storeName = $location[0];
			}

           	$table_save_data = $wpdb->prefix . 'POS_Stores';
		    $get_stores = $wpdb->get_results( "SELECT * FROM $table_save_data WHERE name = '$storeName'" );
		    $file = $get_stores[0]->file;
		    if ( $file != '' ) {
			    chmod($file, 0777);
				$fileHandle = fopen($file,"r");
				
				if ( $fileHandle == true ) {
					$update = array();
					while (($row = fgetcsv($fileHandle)) !== FALSE) {
						if ($row[0] == $sku) {
							if ($row[1] <= 0 || $row[1] < $cart_item['quantity']) {
								wc_add_notice(__('Please edit your cart item <b>'.$product->get_title().'</b> not have quantity in the store'), 'error');
							}
						}
					}
				}else{
					wc_add_notice(__('There is some technical issue please check back soon.'), 'error');
				}
			}
        }
    }
}