<?php
function store_send_order_to_pos( $order_id ){
  if ( is_admin() ) {
    global $wpdb;

    $order = new WC_Order($order_id);
	//echo '<pre>'; print_r($order); die;
    $userId = $order->get_customer_id();

    // $order->get_payment_method(); //GET PAYMENT TITLE
    $paymenTitle = $order->get_payment_method_title(); //GET PAYMENT TITLE

    if ($paymenTitle=="Direct bank transfer") {
      $paymentType = "bank_transfer";
    }elseif ($paymenTitle=="Cash on delivery") {
      $paymentType = "cash";
    }elseif ($paymenTitle=="Check payments") {
      $paymentType = "cheque";
    }else{
      $paymentType = "other";
    }

    $items = $order->get_items();
	  
    $productsArray = array(); //define array to for store products
    $bundle_loop = 0;
    foreach ( $items as $item ) {
      $product  = $item->get_product();
	    $price  = $product->get_price();
      $variation_id = $item->get_variation_id();
      if ( $variation_id != 0 ) {
        $product = wc_get_product($variation_id);
        $sku = $product->get_sku();
      }else{
        $sku = $product->get_sku();
      }
		
      $productName = $item->get_name();
      $productId = $item->get_product_id();
      $quantity = $item->get_quantity();
      $total = $item->get_total();
      $total_tax = $item->get_total_tax();
      $item_meta_data = $item->get_meta_data();

      if ($product->get_type() == "bundle") {
        $bundle_key = $bundle_loop;
        $bundle_meta_data = $item->get_meta( '_stamp', true );
        array_push($productsArray, [
          "type"=>$product->get_type(),
          "item_tax"=>0,
          "name"=>$productName,
          "sku"=>$sku,
          "quantity"=>$quantity,
          "unit_price"=>$price,
          "unit_price_inc_tax"=>$price,
          "product_unit_id"=>1,
          "base_unit_multiplier"=>0,
          "enable_stock"=>1,
          "backorder"=>0,
		      "subtotal"=>$total_tax+$total,
          "product_data"=>[]
        ]);
        $bundle_IDs = [];
        foreach ($bundle_meta_data as $bundle_meta) {
          $bundle_IDs[] = $bundle_meta['product_id'];
        }
      }

      // CHECK IF BUNDLE GET STORE
      $store = $item->get_meta('Store');

      if (!empty($store['bundle_location_id'])) {
        foreach ($store as $key => $bundleItem) {
          foreach ($bundleItem as $key => $value) {
            if ($productId == $key) {
              $store = json_decode($value);
              $storeName = $store[0];
              $storeID = $store[2];
              break 2;
            }
          }
        }
        if (empty($storeName)) {
            $store = json_decode($store['pos_store_location_ID']);
            $storeName = $store[0];
            $storeID = $store[2];
        }
      }else{
        $store = json_decode($store['pos_store_location_ID']);
        $storeName = $store[0];
        $storeID = $store[2];
      }

      if ( !empty($bundle_IDs) && in_array($productId, $bundle_IDs)) {
        if (($key = array_search($productId, $bundle_IDs)) !== false) {
          unset($bundle_IDs[$key]);
        }
        array_push($productsArray[$bundle_key]['product_data'],
          array(
            "item_tax"=>0,
            "sku"=>$sku,
            "quantity"=>$quantity,
            "storeName"=>$storeName,
            "location_id"=>$storeID,
            "product_unit_id"=>1,
            "base_unit_multiplier"=>0,
            "enable_stock"=>1,
            "backorder"=>0
          )
        );
      }else{
        if ($storeName != '') {
          array_push($productsArray,
            array(
              "item_tax"=>0,
              "sku"=>$sku,
              "quantity"=>$quantity,
              "unit_price"=>$price,
              "storeName"=>$storeName,
              "location_id"=>$storeID,
              "unit_price_inc_tax"=>$price,
              "product_unit_id"=>1,
              "base_unit_multiplier"=>0,
              "enable_stock"=>1,
              "backorder"=>0,
			        "subtotal"=>$total_tax+$total
            )
          );
        }
        $bundle_loop++;
      }
    }

    $orderNo = $order_id;
    $paymentType = $paymentType;
    $transaction_date = $order->order_date;
    $firstName = $order->get_shipping_first_name();
    $lastName = $order->get_shipping_last_name();
    $addressLine1 = $order->get_shipping_address_1();
    $addressLine2 = $order->get_shipping_address_2();
    $city = $order->get_shipping_city();
    $postcode = $order->get_shipping_postcode();
    $country = WC()->countries->countries[ $order->get_shipping_country() ];
    $phone = $order->get_billing_phone();
    $email = $order->get_billing_email();
    $order_status = $order->get_status();

    $store = $order->get_meta('_shipping_pickup_pos_stores');
    $table = $wpdb->prefix . 'POS_Stores';
    $get_data = $wpdb->get_results("SELECT pos_id FROM $table WHERE name='$store'");
    $store_location_id = $get_data[0]->pos_id;

    $final = array(
      "orderNo"=>$orderNo,
      "transaction_date"=>$transaction_date,
      "final_total"=>0,
      "status"=>$order_status,
      "payment_status"=>$paymentType,
      "products"=>$productsArray,
      "customer"=>array(
          "firstName"=>$firstName,
          "lastName"=>$lastName,
          "addressLine1"=>$addressLine1,
          "addressLine2"=>$addressLine2,
          "city"=>$city,
          "postcode"=>$postcode,
          "country"=>$country,
          "phone"=>$phone,
          "email"=>$email
        ),
      "payment"=>array(array(
          "amount"=>0,
          "method"=>$paymentType,
          "card_number"=>"",
          "card_type"=>"",
          "card_holder_name"=>"",
          "card_month"=>"",
          "card_security"=>"",
          "cheque_number"=>"",
          "bank_account_number"=>"",
          "card_transaction_number"=>"",
          "note"=>""
        )),
      "collection"=>array(
        "location_id"=>$store_location_id
      )
    );

    $final = json_encode($final);
    $table_save_data = $wpdb->prefix . 'POS_Stores_sync_setting';
    $get_data = $wpdb->get_results("SELECT * FROM $table_save_data");
    if (!empty($get_data)) {
      $url = $get_data[0]->url;
      $key1 = $get_data[0]->key1;
      $key2 = $get_data[0]->key2;

      $auth = base64_encode($key1.":".$key2);
    }else{
      $url = '';
      $key1 = '';
      $key2 = '';
    }

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => $url."/woocommerce/apiOrderSync",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => $final,
      CURLOPT_HTTPHEADER => array(
        "Accept: application/json",
        "Authorization: Basic ".$auth,
        "Content-Type: application/json",
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      store_error_log("error","cURL Error #:" . $err);
    }else {
      $response = json_decode($response,TRUE);
      $msg = "orderNo : ".$orderNo." || ".$response['msg'];
      store_error_log("notice",$msg);
      if ($response['status'] == 200) {
        // update_quantity_from_csv($productsArray);
        update_post_meta( $order_id, 'store_order_sync_to_pos', 'complete' );
      }elseif ($response['msg'] == 'Order already exist') {
        update_post_meta( $order_id, 'store_order_sync_to_pos', 'complete' );
      }
    }
  }
}
add_action('woocommerce_order_status_processing','store_send_order_to_pos');