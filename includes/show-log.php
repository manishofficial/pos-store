<?php
if ($_REQUEST['log-action'] == 'clear') {
    unlink(dirname( plugin_dir_path(__FILE__) ).'/logs/debug.log');
    error_log('', 3, dirname( plugin_dir_path(__FILE__) ).'/logs/debug.log');
    wp_redirect(home_url( '/wp-admin/admin.php?page=store_log'));
}
?>
<div class="container">
    <div class="wrap">
        <h1 class="wp-heading-inline">Logs</h1><a href="<?php echo home_url( '/wp-admin/admin.php?page=store_log&log-action=clear' ); ?>" class="page-title-action">Clear log</a>
    </div>
    <div class='logDiv' style='position: absolute; background-color: #fff; overflow: scroll; font-weight: 900; height: 400px; width: 98%;'>
        <?php
            $logFile = dirname( plugin_dir_path(__FILE__) ).'/logs/debug.log';
            if (file_exists($logFile)) {
                $file = file($logFile);
                $file = array_reverse($file);
                foreach($file as $f){
                    echo $f."<br />";
                }
            }else{
                echo "<div class='notice notice-warning is-dismissible'>
                        <p>Store POS Log File Is Not Exist </p>
                      </div>";
            }
        ?>
    </div>
</div>