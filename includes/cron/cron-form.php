<div class="container">
    <h1>Setup Cron</h1>
    <div id="cron_data"></div>
    <form method="post" id="cron_form">
      <table>
        <tr>
            <td><label">Daily</label></td>
            <td><input type="radio" name="schedule_type" id="schedule_type" required value="daily"></td>
            <td><input type="time" name="time_daily" id="time_daily" class="removeFocus"></td>
        </tr>
        <tr>
            <td><label>Twice Daily</label></td>
            <td><input type="radio" name="schedule_type" id="schedule_type" required value="twicedaily" /></td>
            <td><input type="time" name="time_one" id="time_one" class="removeFocus"></td>
            <td><input type="time" name="time_two" id="time_two" class="removeFocus"></td>
            <!-- <td>NOTE : Cron Run Every Twelve Hour Per Day</td> -->
        </tr>
        <tr>
            <td><label>Weekly</label></td>
            <td><input type="radio" name="schedule_type" id="schedule_type" required value="weekly" /></td>
            <td><select name="day" id="day_weekly">
                <option value="sunday">Sunday</option>
                <option value="monday">Monday</option>
                <option value="tuesday">Tuesday</option>
                <option value="wednesday">Wednesday</option>
                <option value="thursday">Thursday</option>
                <option value="friday">Friday</option>
                <option value="saturday">Saturday</option>
            </select></td>
            <td><input type="time" name="time_one" id="time_weekly" class="removeFocus"></td>
        </tr>
        <tr>
            <td><label>Monthly</label></td>
            <td><input type="radio" name="schedule_type" id="schedule_type" required value="monthly" /></td>
            <td><select name="day" id="day_monthly">
                <option value="sunday">Sunday</option>
                <option value="monday">Monday</option>
                <option value="tuesday">Tuesday</option>
                <option value="wednesday">Wednesday</option>
                <option value="thursday">Thursday</option>
                <option value="friday">Friday</option>
                <option value="saturday">Saturday</option>
            </select></td>
            <td><input type="time" name="time_one" id="time_monthly" class="removeFocus"></td>
        </tr>
        <tr>
            <td><input type="submit" id="" name="sync_Schedule" value="Set Schedule" class="button-primary ajax_cron_btn" data-loading-text="Please Wait..."></td>
        </tr>
      </table>
    </form>

    <hr>
    <?php $gif = dirname(dirname(plugin_dir_url( __FILE__ ))).'/assets/images/'; ?>
    <div>
        <div id="get-cron"></div>
        <div id="spinner"><img src="<?php echo $gif.'loading-data.gif'; ?>" width="80px"></div>
    </div>
</div>
<script>
    function fieldBlankFocus(value){
        jQuery(".removeFocus").css('border','');
        jQuery("#"+value).focus();
        jQuery("#"+value).css('border','solid 1px red');
        jQuery("#"+value).change(function(){
            jQuery("#"+value).css('border','');
        });
    }
    // GET CRON
    jQuery('#get-cron').load("<?php echo plugin_dir_url( __FILE__ ).'/get-cron.php'; ?>",null,showResponceData);

    function showResponceData(){
        jQuery('#spinner').hide();
        jQuery('#get-cron').show();
    }

    //Submit cron form
    jQuery("form").on("submit", function (e) {
        e.preventDefault();
        var data =[];
        var schedule_type = jQuery("#schedule_type:checked").val();

        switch(schedule_type){
            case 'daily':
                var time_daily = jQuery("#time_daily").val();
                if (time_daily == '') {
                    fieldBlankFocus("time_daily");
                    return false;
                }
                data.push("daily" , time_daily);
            break;
            case 'twicedaily':
                var time_one = jQuery("#time_one").val();
                if (time_one == '') {
                    fieldBlankFocus("time_one");
                    return false;
                }
                var time_two = jQuery("#time_two").val();
                if (time_two == '') {
                    fieldBlankFocus("time_two");
                    return false;
                }
                if ( time_one == time_two ) {
                    jQuery('#cron_data').html("<div class='notice notice-error is-dismissible' ><p>Please set different sync timings for twice daily sync schedule.</p></div>");
                    fieldBlankFocus("time_one");
                    fieldBlankFocus("time_two");
                    return false;
                }
                data.push("twicedaily", time_one ,time_two);
            break;
            case 'weekly':
                var day_monthly = jQuery("#day_weekly").val();
                var time_weekly = jQuery("#time_weekly").val();
                if (time_weekly == '') {
                    fieldBlankFocus("time_weekly");
                }
                data.push("weekly", day_monthly ,time_weekly);
            break;
            case 'monthly':
                var day_monthly = jQuery("#day_monthly").val();
                var time_monthly = jQuery("#time_monthly").val();
                if (time_monthly == '') {
                    fieldBlankFocus("time_monthly");
                }
                data.push("monthly", day_monthly ,time_monthly);
            break;
        }
        jQuery('#spinner').show();
        jQuery('#get-cron').hide();
        if (data[1] == '' || data[2] == '') {
            return false;
        }

        jQuery.post("<?php echo plugin_dir_url(__FILE__).'cron.php'; ?>", { data },function(data) {
            jQuery('#spinner').show();
            jQuery('#get-cron').hide();
            jQuery('#get-cron').load("<?php echo plugin_dir_url( __FILE__ ).'/get-cron.php'; ?>",null,showResponceData);
            setTimeout(function() {
             jQuery(".ajax_cron_btn").prop("disabled", false);
             jQuery(".ajax_cron_btn").attr('style', 'cursor:pointer');
            }, 600000);

            jQuery('#cron_data').html(data);
            jQuery('#cron_form')[0] && jQuery('#cron_form')[0].reset();
        });
    });
</script>