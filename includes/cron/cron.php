<?php

$root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
require_once( $root.'/wp-load.php' );

// Set Woocommerce TimeZone
$timezone = get_option('timezone_string');
date_default_timezone_set($timezone);

$Array = $_POST['data'];

if(empty($Array[0])) {
    echo "<div class='notice notice-success is-dismissible'>
		    <p>Please Select Cron First.</p>
		  </div>";
    die();
}

//get cron Time
$crons  = _get_cron_array();
$events = array();
foreach ( $crons as $time => $cron ) {
	foreach ( $cron as $hook => $dings ) {
		foreach ( $dings as $sig => $data ) {
			# This is a prime candidate for a Crontrol_Event class but I'm not bothering currently.
			$events[ "$hook" ] = (object) array(
				'hook'     => $hook,
				'time'     => $time,
				'sig'      => $sig,
				'args'     => $data['args'],
				'schedule' => $data['schedule'],
				'interval' => isset( $data['interval'] ) ? $data['interval'] : null,
			);
		}
	}
}

$daily_time = strtotime(date('d-M-Y G:i',$events["pos_store_daily"]->time));
$time_twic1 = strtotime(date('d-M-Y G:i',$events["pos_store_twicedaily_1"]->time));
$time_twic2 = strtotime(date('d-M-Y G:i',$events["pos_store_twicedaily_2"]->time));
$time_weekly = strtotime(date('d-M-Y G:i',$events["pos_store_weekly"]->time));
$time_monthly = strtotime(date('d-M-Y G:i',$events["pos_store_monthly"]->time));

// SETUP THE CRON AS PER SCHEDULE
if($Array[0]=="daily"){
	if (wp_next_scheduled ( 'pos_store_twicedaily_1' )) {
			wp_clear_scheduled_hook('pos_store_twicedaily_1');
	}
	if (wp_next_scheduled ( 'pos_store_twicedaily_2' )) {
			wp_clear_scheduled_hook('pos_store_twicedaily_2');
	}
	if (wp_next_scheduled ( 'pos_store_weekly' )) {
			wp_clear_scheduled_hook('pos_store_weekly');
	}
	if (wp_next_scheduled ( 'pos_store_monthly' )) {
			wp_clear_scheduled_hook('pos_store_monthly');
	}

	if ( date('H:i') > date('H:i' , strtotime($Array[1])) ) {
		$time_daily = date('H:i d-M-Y' , strtotime($Array[1]."+1 day"));
	}else{
		$time_daily = date('H:i d-M-Y' , strtotime($Array[1]));
	}
	$chk_time = strtotime(date('d-M-Y').$time_daily);
	if (! wp_next_scheduled ( 'pos_store_daily' )) {
		wp_schedule_event( strtotime($time_daily), 'daily', 'pos_store_daily');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	elseif ($daily_time != $chk_time) {
		wp_clear_scheduled_hook('pos_store_daily');
		wp_schedule_event( strtotime($time_daily), 'daily', 'pos_store_daily');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	else{
		echo "<div class='notice notice-error is-dismissible'>
			    <p>Schedule already exists.</p>
			  </div>";
	}
}

if($Array[0]=="twicedaily"){
	$time_twice_one = $Array[1];
	$time_twice_two = $Array[2];
	$chk_time_twice1 = strtotime(date('d-M-Y').$time_twice_one);
	$chk_time_twice2 = strtotime(date('d-M-Y').$time_twice_two);
	$chk_time_twice_one = strtotime(date('d-M-Y').$time_twice_one);
	$chk_time_twice_two = strtotime(date('d-M-Y').$time_twice_two);
	if (wp_next_scheduled ( 'pos_store_daily' )) {
			wp_clear_scheduled_hook('pos_store_daily');
	}
	if (wp_next_scheduled ( 'pos_store_weekly' )) {
			wp_clear_scheduled_hook('pos_store_weekly');
	}
	if (wp_next_scheduled ( 'pos_store_monthly' )) {
			wp_clear_scheduled_hook('pos_store_monthly');
	}

	if (! wp_next_scheduled ( 'pos_store_twicedaily_1' )) {
		wp_schedule_event( strtotime($time_twice_one), "daily", 'pos_store_twicedaily_1');

		if (! wp_next_scheduled ( 'pos_store_twicedaily_2' )) {
			wp_schedule_event( strtotime($time_twice_two), "daily", 'pos_store_twicedaily_2');
			echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
		}
	}
	elseif ($time_twic1 != $chk_time_twice1) {
		wp_clear_scheduled_hook('pos_store_twicedaily_1');
		wp_schedule_event( strtotime($time_twice_one), "daily", 'pos_store_twicedaily_1');

		if ($time_twic2 != $chk_time_twice2) {
			wp_clear_scheduled_hook('pos_store_twicedaily_2');
			wp_schedule_event( strtotime($time_twice_two), "daily", 'pos_store_twicedaily_2');
		}
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	else{
		echo "<div class='notice notice-error is-dismissible'>
			    <p>Schedule already exists.</p>
			  </div>";
	}
}

if($Array[0]=="weekly"){
	$day_weekly = $Array[1];
	$chk_time_weekly = strtotime(date('d-M-Y').$day_weekly);
	if (wp_next_scheduled ( 'pos_store_daily' )) {
			wp_clear_scheduled_hook('pos_store_daily');
	}
	if (wp_next_scheduled ( 'pos_store_twicedaily_1' )) {
			wp_clear_scheduled_hook('pos_store_twicedaily_1');
	}
	if (wp_next_scheduled ( 'pos_store_twicedaily_2' )) {
			wp_clear_scheduled_hook('pos_store_twicedaily_2');
	}
	if (wp_next_scheduled ( 'pos_store_monthly' )) {
			wp_clear_scheduled_hook('pos_store_monthly');
	}

	$times = explode(":", $Array[2]);
	$hours = $times[0];
	$minutes = $times[1];
	$day = $Array[1];
	$weeklyDate = strtotime("next ".$day);
	$weeklyDate += ($hours*3600)+($minutes*60);
	$chk_time = strtotime(date('d-M-Y').$weeklyDate);

	if (! wp_next_scheduled ( 'pos_store_weekly' )) {
		wp_schedule_event( $weeklyDate, $Array[0], 'pos_store_weekly');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
			  die();
	}
	elseif ($time_weekly != $weeklyDate) {
		wp_clear_scheduled_hook('pos_store_weekly');
		wp_schedule_event( $weeklyDate, $Array[0], 'pos_store_weekly');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	else{
		echo "<div class='notice notice-error is-dismissible'>
			    <p>Schedule already exists.</p>
			  </div>";
	}
}

if($Array[0]=="monthly"){
	if (wp_next_scheduled ( 'pos_store_daily' )) {
			wp_clear_scheduled_hook('pos_store_daily');
	}
	if (wp_next_scheduled ( 'pos_store_twicedaily_1' )) {
			wp_clear_scheduled_hook('pos_store_twicedaily_1');
	}
	if (wp_next_scheduled ( 'pos_store_twicedaily_2' )) {
			wp_clear_scheduled_hook('pos_store_twicedaily_2');
	}
	if (wp_next_scheduled ( 'pos_store_weekly' )) {
			wp_clear_scheduled_hook('pos_store_weekly');
	}
	$times = explode(":", $Array[2]);
	$hours = $times[0];
	$minutes = $times[1];
	$day = $Array[1];
	$monthDate = strtotime("next ".$day);
	$monthDate += ($hours*3600)+($minutes*60);
	$chk_time = strtotime(date('d-M-Y').$monthDate);

	if (! wp_next_scheduled ( 'pos_store_monthly' )) {
		wp_schedule_event( $monthDate, $Array[0], 'pos_store_monthly');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	elseif ($time_monthly != $monthDate) {
		wp_clear_scheduled_hook('pos_store_monthly');
		wp_schedule_event( $monthDate, $Array[0], 'pos_store_monthly');
		echo "<div class='notice notice-success is-dismissible'>
			    <p>Schedule Created.</p>
			  </div>";
	}
	else{
		echo "<div class='notice notice-error is-dismissible'>
			    <p>Schedule already exists.</p>
			  </div>";
	}
}
