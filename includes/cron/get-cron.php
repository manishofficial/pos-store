<?php
  $root = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
  require_once $root.'/wp-load.php';

  $crons  = _get_cron_array();
  $events = array();
  foreach ( $crons as $time => $cron ) {
    foreach ( $cron as $hook => $dings ) {
      foreach ( $dings as $sig => $data ) {
        # This is a prime candidate for a Crontrol_Event class but I'm not bothering currently.
        $events[ "$hook" ] = (object) array(
          'hook'     => $hook,
          'time'     => $time,
          'sig'      => $sig,
          'args'     => $data['args'],
          'schedule' => $data['schedule'],
          'interval' => isset( $data['interval'] ) ? $data['interval'] : null,
        );
      }
    }
  }
  function cron_job_delete($cron_type){
    wp_clear_scheduled_hook($cron_type);
  }

  echo "<h3>Current Schedule</h3>";

  if(!empty($events)){
    if (isset($events["pos_store_daily"]) && $events["pos_store_daily"]->hook=="pos_store_daily") {
      echo "<div class='cronData'>Daily sync scheduled for store and data on ".date('d-M-Y l', $events["pos_store_daily"]->time)." at ".date('h:i A', $events["pos_store_daily"]->time)." <a href='".plugin_dir_url( __FILE__ )."delete-cron.php?delte-cron=pos_store_daily' class='submitdelete'>Delete</a></div>";
    }
    elseif (isset($events["pos_store_twicedaily_1"]) && $events["pos_store_twicedaily_1"]->hook=="pos_store_twicedaily_1") {
      echo "<div class='cronData'>Daily sync scheduled for store and data on ".date('d-M-Y l', $events["pos_store_twicedaily_1"]->time)." at ".date('h:i A', $events["pos_store_twicedaily_1"]->time);
      if (isset($events["pos_store_twicedaily_2"]) && $events["pos_store_twicedaily_2"]->hook=="pos_store_twicedaily_2") {
          echo " and at ".date('h:i A', $events["pos_store_twicedaily_2"]->time)." <a href='".plugin_dir_url( __FILE__ )."delete-cron.php?delte-cron=pos_store_twicedaily_1' class='submitdelete'>Delete</a></div>";
      }
    }
    elseif (isset($events["pos_store_weekly"]) && $events["pos_store_weekly"]->hook=="pos_store_weekly") {
      echo "<div class='cronData'>Weekly sync scheduled for store and data on ".date('d-M-Y l', $events["pos_store_weekly"]->time)." at ".date('h:i A', $events["pos_store_weekly"]->time)." <a href='".plugin_dir_url( __FILE__ )."delete-cron.php?delte-cron=pos_store_weekly' class='submitdelete'>Delete</a></div>";
    }
    elseif (isset($events["pos_store_monthly"]) && $events["pos_store_monthly"]->hook=="pos_store_monthly") {
      echo "<div class='cronData'>Monthly sync scheduled for store and data on ".date('d-M-Y l', $events["pos_store_monthly"]->time)." at ".date('h:i A', $events["pos_store_monthly"]->time)." <a href='".plugin_dir_url( __FILE__ )."delete-cron.php?delte-cron=pos_store_monthly' class='submitdelete'>Delete</a></div>";
    }
    else{
      echo "<b style='color:red;'>No sync scheduled yet.</b>";
    }
  }

